package com.gameware.tcc2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.gameware.tcc2.model.Cliente;
import com.gameware.tcc2.model.Status;
import com.gameware.tcc2.utils.CelulaREST;
import com.gameware.tcc2.utils.Constants;
import com.gameware.tcc2.utils.GamewareUtils;

public class TelaRedefinirSenhaGerente extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_redefinir_senha_gerente);
		
		List<String> listaGerente = new ArrayList<String>();
		List<Cliente> gerentesList = new ArrayList<Cliente>();
		try {
			gerentesList= CelulaREST.listaGerentes();
		} catch (Exception e) {
			e.printStackTrace();
		}
		final List<Cliente>listaClienteResponse = gerentesList;
		
		
		//TODO: Lista de clientes
		for (Iterator iterator = listaClienteResponse.iterator(); iterator.hasNext();) {
			Cliente cliente = (Cliente) iterator.next();
			listaGerente.add(cliente.getNome());
		}
		
		
		Spinner s = (Spinner) findViewById(R.id.seletor_gerente);
		s.setOnItemSelectedListener(new OnItemSelectedListener() {

			Intent it = getIntent();
			Bundle bundle = new Bundle();
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				//TODO: Verificar se o selectedItemPosition do spinner esta correto
				bundle.putInt(Constants.SESSION_INDICE_SELECIONADO, listaClienteResponse.get(arg2).getIdUsuario());
				it.putExtras(bundle);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}

		});
		
		@SuppressWarnings("unchecked")
		ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, listaGerente);
        s.setAdapter(adapter);
	}

	public void onButtonClick(View v){
		EditText novaSenha = (EditText) findViewById(R.id.text_nova_senha);
		EditText novaSenhaConfirm = (EditText) findViewById(R.id.text_confirme_nova_senha);
		boolean valido = true;
		
		if (GamewareUtils.campoVazio(novaSenha)){
			Toast.makeText(TelaRedefinirSenhaGerente.this, "Voc� deve preencher o campo 'Nova senha'", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(novaSenhaConfirm)){
			Toast.makeText(TelaRedefinirSenhaGerente.this, "Voc� deve preencher o campo 'Confirme a nova senha'", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		
		if (valido){
			if (!novaSenha.getText().toString().equals(novaSenhaConfirm.getText().toString())){
				Toast.makeText(TelaRedefinirSenhaGerente.this, "Senhas n�o conferem", Toast.LENGTH_SHORT).show();
				valido=false;
			}
		}
		
		if (valido) {
			if (R.id.button_ok == v.getId()) {
				alertaMessageConfirm("Confirmar altera��o?", "Senha alterada com sucesso!");
		 }
		}
	}
	
	public void alertaMessageOk(String mensagem){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// set dialog message
		alertDialogBuilder
			.setMessage(mensagem)
			.setCancelable(false)
			.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					finish();
				}
			  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
	}
	
	public void alertaMessageErro(String mensagem){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// set dialog message
		alertDialogBuilder
			.setMessage(mensagem)
			.setCancelable(false)
			.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.cancel();
				}
			  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
	}
	
	public void alertaMessageConfirm (String mensagemQuestion, final String mensagemSucess){

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(mensagemQuestion)
				.setCancelable(false)
				.setPositiveButton(Constants.BUTTON_SIM,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								String mensagemSucessAux = mensagemSucess;
								Spinner seletor_gerente = (Spinner) findViewById(R.id.seletor_gerente);
								EditText novaSenha = (EditText) findViewById(R.id.text_nova_senha);
								
								Intent it = getIntent();
								Bundle bundle = it.getExtras();
								
								Integer indiceSelecionado = bundle.getInt(Constants.SESSION_INDICE_SELECIONADO);
								
								Status status = new Status();
								try {
									status = CelulaREST.redefinirSenhaGerente(indiceSelecionado, GamewareUtils.stringEditText(novaSenha));
								} catch (JSONException e) {
									status.setCodigoErro(Constants.ERRO_OPERACAO_BD);
								}
								
								if (Constants.SUCESSO_OPERACAO_BD.equals(status.getCodigoErro())){
									
								}else if (Constants.ERRO_OPERACAO_BD.equals(status.getCodigoErro())){
									alertaMessageErro(Constants.MESSAGE_ERRO_BD);
								}
								final String mensagemAux2 = mensagemSucessAux;
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TelaRedefinirSenhaGerente.this);
									// set dialog message
									alertDialogBuilder
										.setMessage(mensagemAux2)
										.setCancelable(false)
										.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) {
												if (Constants.MESSAGE_ERRO_BD.equals(mensagemAux2)){
													dialog.cancel();
												}else{
													finish();
												}
											}
										  });
						 
										// create alert dialog
										AlertDialog alertDialog = alertDialogBuilder.create();
						 
										// show it
										alertDialog.show();
							}
						})
				.setNegativeButton(Constants.BUTTON_NAO,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		super.finish();
	}
}
