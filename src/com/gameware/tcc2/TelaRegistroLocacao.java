package com.gameware.tcc2;

import java.util.ArrayList;
import java.util.List;

import com.gameware.tcc2.utils.Constants;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class TelaRegistroLocacao extends Activity {

	private static final String TAG_CONSOLE = "consoleName";
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_registro_locacao);
		
        // getting intent data
        Intent in = getIntent();
        
        // Get JSON values from previous intent
        String consoleName = in.getStringExtra(TAG_CONSOLE);
		System.out.println(consoleName);
		
		List<String> listaCliente = new ArrayList<String>();
		listaCliente.add("Fabio Eduardo");
		listaCliente.add("Mirosmar Jos� de Camargo");
		listaCliente.add("Carlos Ricardo");
		listaCliente.add("Fernando Locatelli");
		Spinner s = (Spinner) findViewById(R.id.seletor_cliente);
		
		List<String> listaJogo = new ArrayList<String>();
		listaJogo.add("Wolfenstein 3D");
		listaJogo.add("Quake 3 Arena");
		listaJogo.add("Stunts");
		listaJogo.add("NBA Jam");
		Spinner s1 = (Spinner) findViewById(R.id.seletor_jogo);
		
		ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, listaCliente);
        s.setAdapter(adapter);
        
		ArrayAdapter adapter2 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, listaJogo);
        s1.setAdapter(adapter2);
		
		List<String> listaMinutos = new ArrayList<String>();
		listaMinutos.add("00");
		listaMinutos.add("15");
		listaMinutos.add("30");
		listaMinutos.add("45");
        Spinner s2 = (Spinner) findViewById(R.id.seletor_minuto);
        
		ArrayAdapter adapter1 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, listaMinutos);
        s2.setAdapter(adapter1);
        
	}
	
	public void onButtonClick(View v){
		CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox_livre);
		Spinner seletorMinuto = (Spinner) findViewById(R.id.seletor_minuto);
		EditText seletorHoras = (EditText) findViewById(R.id.seletor_hora);
		if (v.getId()==R.id.checkBox_livre){
			if (checkBox.isChecked()){
				seletorMinuto.setEnabled(false);
				seletorHoras.setEnabled(false);
			}
			else{
				seletorMinuto.setEnabled(true);
				seletorHoras.setEnabled(true);
			}
		}else if (v.getId()==R.id.button_ok){
			boolean camposOk = false;
			if (!checkBox.isChecked()){
			/*Verifica campos obrigatorios*/
			if ( (seletorMinuto.getSelectedItem().toString().equals("00")) && (seletorHoras.getText().toString().equals(Constants.EMPTY)) ){
				Toast.makeText(TelaRegistroLocacao.this, "Informe o tempo de uso desejado", Toast.LENGTH_SHORT).show();
			}else{
				camposOk = true;
			}
			
			if (camposOk){
				String valorPagar = "20,00";
				alertMessage(valorPagar);
			 }
			}else if (checkBox.isChecked()){
				Intent in = new Intent(getApplicationContext(), TelaMenuGerenteActivity.class);
				startActivity(in);
			}
		}
	}
	
	public void alertMessage(final String valorPagar){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				TelaRegistroLocacao.this);
 
			// set dialog message
			alertDialogBuilder
				.setTitle("Valor a pagar")
				.setMessage("R$"+valorPagar)
				.setCancelable(false)
				.setNeutralButton("OK",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						finish();
						Intent in = new Intent(getApplicationContext(), TelaMenuGerenteActivity.class);
						startActivity(in);
					}
				  });
 
				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();
 
				// show it
				alertDialog.show();
	}
	@Override
	public void onDestroy(){
		super.onDestroy();
		super.finish();
	}
}
