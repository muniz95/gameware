package com.gameware.tcc2;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListView;

import com.gameware.tcc2.menu.CustomArrayAdapter;
import com.gameware.tcc2.menu.DataModel;
import com.gameware.tcc2.menu.GroupHeaderInfo;
import com.gameware.tcc2.menu.RowInfo;
import com.gameware.tcc2.utils.Constants;

public class TelaMenuAdministradorActivity extends ListActivity {
    private DrawerLayout mDrawerLayout;
    private ExpandableListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private CustomArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_menu_administrador);

        mTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ExpandableListView) findViewById(R.id.left_drawer);


        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        adapter = new CustomArrayAdapter(initData(),this);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        //listen to child click event
        mDrawerList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                    int groupPosition, int childPosition, long id) {
            	if (groupPosition==Constants.MENU_GROUP_GER_GERENTES){
            		if (childPosition==Constants.MENU_ITEM_DADOS_CADASTRAIS_GERENTE){
            			mDrawerLayout.closeDrawer(mDrawerList);
            			Intent it = new Intent(getApplicationContext(),TelaSelecaoGerenteDadosCadastrais.class);
            			startActivity(it);
            		}else if (childPosition==Constants.MENU_ITEM_REDEFINIR_SENHA_GERENTE){
            			mDrawerLayout.closeDrawer(mDrawerList);
            			Intent it = new Intent(getApplicationContext(),TelaRedefinirSenhaGerente.class);
            			startActivity(it);
            		}else if (childPosition==Constants.MENU_ITEM_CADASTRO_GERENTE){
            			mDrawerLayout.closeDrawer(mDrawerList);
            			Intent it = new Intent(getApplicationContext(),TelaCadastroGerenteActivity.class);
            			startActivity(it);
            		}
            	}else if (groupPosition==Constants.MENU_GROUP_GER_CONSOLES){
            		if (childPosition==Constants.MENU_ITEM_VALOR_UTILIZACAO_CONSOLE){
            			mDrawerLayout.closeDrawer(mDrawerList);
            			Intent it = new Intent(getApplicationContext(),TelaHoraConsole.class);
            			startActivity(it);            			
            		}else if (childPosition==Constants.MENU_ITEM_CADASTRO_CONSOLE){
            			mDrawerLayout.closeDrawer(mDrawerList);
            			Intent it = new Intent(getApplicationContext(),TelaCadastroConsole.class);
            			startActivity(it);      			
            		}
            	}else if (groupPosition==Constants.MENU_GROUP_DESCONTOS){
            		if (childPosition==Constants.MENU_ITEM_GER_DESCONTO){
            			mDrawerLayout.closeDrawer(mDrawerList);
            			Intent it = new Intent(getApplicationContext(),TelaSelecaoDescontoActivity.class);
            			startActivity(it);          			
            		}else if (childPosition==Constants.MENU_ITEM_CADASTRO_DESCONTO){
            			mDrawerLayout.closeDrawer(mDrawerList);
            			Intent it = new Intent(getApplicationContext(),TelaCadastroDescontoActivity.class);
            			startActivity(it);          			
            		}
            	}
				return false;
            }
        });

        mDrawerList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                return false;
            }
        });

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
                ) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(0);
        }
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.tela_menu_gerente, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
//        menu.findItem(R.id.action_logout).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         // The action bar home/up action should open or close the drawer.
         // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {
        case R.id.action_logout:
		    AlertDialog.Builder builder = new AlertDialog.Builder(this);
		    builder.setMessage("Voc� tem certeza que deseja sair?")
		           .setCancelable(false)
		           .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		       			finish();
		               }
		           })
		           .setNegativeButton("N�o", new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		                    dialog.cancel();
		               }
		           });
		    AlertDialog alert = builder.create();
		    alert.show();
        }
		return false;
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        	System.out.println("Posicao onItemClick: id "+id+" position "+position);
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        // update the main content by replacing fragments
    	System.out.println("Posicao selectItem: "+position);
//        FragmentManager fragmentManager = getFragmentManager();
//        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
//        setTitle(mPlanetTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
    	title = "Gameware";
        mTitle = title;
        getActionBar().setTitle("Gameware");
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private DataModel initData()
    {
        ArrayList<GroupHeaderInfo> headArray = new ArrayList<GroupHeaderInfo>();
        ArrayList<ArrayList<RowInfo>> rowsArray = new ArrayList<ArrayList<RowInfo>>();

        //Asian country
        {
            GroupHeaderInfo info = new GroupHeaderInfo("Gerenciamento de gerentes", R.drawable.ic_gerent);
            headArray.add(info);
            ArrayList<RowInfo> rowArray = new ArrayList<RowInfo>();
            RowInfo row = new RowInfo("Dados cadastrais");
            rowArray.add(row);
            RowInfo row2 = new RowInfo("Redefinir senha");
            rowArray.add(row2);
            RowInfo row3 = new RowInfo("Cadastrar novo gerente");
            rowArray.add(row3);
            rowsArray.add(rowArray);
        }

        {
            GroupHeaderInfo info2 = new GroupHeaderInfo("Gerenciamento de consoles", R.drawable.ic_console_mega_drive);
            headArray.add(info2);

            ArrayList<RowInfo> rowArray = new ArrayList<RowInfo>();
            RowInfo row = new RowInfo("Valor por utiliza��o do console");
            rowArray.add(row);
            RowInfo row2 = new RowInfo("Cadastrar novo console");
            rowArray.add(row2);
            rowsArray.add(rowArray);
        }

        {
            GroupHeaderInfo info3 = new GroupHeaderInfo("Descontos", R.drawable.ic_dollar);
            headArray.add(info3);

            ArrayList<RowInfo> rowArray = new ArrayList<RowInfo>();
            RowInfo row = new RowInfo("Gerenciamento de descontos");
            rowArray.add(row);
            RowInfo row2 = new RowInfo("Cadastrar novo desconto");
            rowArray.add(row2);
            rowsArray.add(rowArray);

        }

        return new DataModel(headArray,rowsArray);
    }
    
	@Override
	protected void onDestroy(){
		super.onDestroy();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage("Voc� tem certeza que deseja sair?")
	           .setCancelable(false)
	           .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	       			finish();
	               }
	           })
	           .setNegativeButton("N�o", new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	                    dialog.cancel();
	               }
	           });
	    AlertDialog alert = builder.create();
	    alert.show();
	}
    
	
}