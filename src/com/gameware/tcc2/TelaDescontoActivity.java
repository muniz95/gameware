package com.gameware.tcc2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.gameware.tcc2.model.Console;
import com.gameware.tcc2.model.ConsoleDesconto;
import com.gameware.tcc2.model.Desconto;
import com.gameware.tcc2.utils.Constants;
import com.gameware.tcc2.utils.GamewareUtils;
import com.gameware.tcc2.utils.MaskUtils;

public class TelaDescontoActivity extends Activity {

	private TextWatcher dtValidadeMask;
	Integer counterConsoles;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_tela_dados_cadastrais_desconto);
		counterConsoles = 0;
		
		final SimpleDateFormat sdf_data = new SimpleDateFormat(Constants.FORMATO_DATA);
		final SimpleDateFormat sdf_hora = new SimpleDateFormat(Constants.FORMATO_HORA);
		final SimpleDateFormat sdf_minuto = new SimpleDateFormat(Constants.FORMATO_MINUTO);
		final EditText text_hora_validade = (EditText)findViewById(R.id.text_hora_validade);
		final EditText text_minutos_validade = (EditText) findViewById(R.id.text_minutos_validade);
		final EditText text_data_validade = (EditText)findViewById(R.id.text_data_validade);
		Spinner seletor_desconto = (Spinner)findViewById(R.id.seletor_desconto);
		final EditText text_porcentagem_desconto = (EditText)findViewById(R.id.text_porcentagem_desconto);
		Button button_consoles_participantes = (Button)findViewById(R.id.button_consoles_participantes);
		
		text_porcentagem_desconto.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus){
					if (!GamewareUtils.campoVazio(text_porcentagem_desconto)){
						if (!text_porcentagem_desconto.getText().toString().contains("%")){
							text_porcentagem_desconto.setText(text_porcentagem_desconto.getText()+"%");
						}
					}
				}
				
			}
		});
		
		//TODO: Chamar lista de descontos
		final List<Desconto> listaDescontosWS = new ArrayList<Desconto>();
		List<String> listaDescontos = new ArrayList<String>();
		
		for (Iterator iterator = listaDescontosWS.iterator(); iterator.hasNext();) {
			Desconto desconto = (Desconto) iterator.next();
			listaDescontos.add(desconto.getNomeDesconto());
		}
		
		seletor_desconto.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				text_data_validade.setText(sdf_data.format(listaDescontosWS.get(arg2).getFimValidade()));
				text_hora_validade.setText(sdf_hora.format(listaDescontosWS.get(arg2).getFimValidade()));
				text_minutos_validade.setText(sdf_minuto.format(listaDescontosWS.get(arg2).getFimValidade()));
				text_porcentagem_desconto.setText(listaDescontosWS.get(arg2).getPorcentagemDesconto()+"%");
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
		});
		

		Integer idDescontoSelecionado = seletor_desconto.getSelectedItemPosition();
		Intent it = getIntent();
		Bundle bundle = it.getExtras();
		//TODO: Chamar servico para verificar quais sao os consoles que estao relacionados ao desconto selecionado
		List<ConsoleDesconto> listaConsoleDescontoWS = new ArrayList<ConsoleDesconto>();
		if (listaConsoleDescontoWS.size()>0){
			bundle.putBoolean(Constants.SESSION_SEM_CONSOLES_APTOS_DESCONTO, true);
		}else{
			bundle.putBoolean(Constants.SESSION_SEM_CONSOLES_APTOS_DESCONTO, false);
		}
		
		dtValidadeMask = MaskUtils.insert(Constants.MASK_FORMAT_DATA,text_data_validade);
		text_data_validade.addTextChangedListener(dtValidadeMask);
		
		text_porcentagem_desconto.setEnabled(false);
		text_hora_validade.setEnabled(false);
		text_minutos_validade.setEnabled(false);
		text_data_validade.setEnabled(false);
		button_consoles_participantes.setEnabled(false);
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(Menu.CATEGORY_ALTERNATIVE, Menu.FIRST, Menu.NONE, Constants.MENU_EDITAR);
		menu.add(Menu.NONE, Menu.FIRST+1, Menu.NONE, Constants.MENU_SALVAR);
		menu.add(Menu.NONE, Menu.FIRST+2, Menu.NONE, Constants.MENU_EXCLUIR_DESCONTO);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		if (item.getItemId()==Menu.FIRST+2){
			alertaMessageConfirmExclusao(Constants.MESSAGE_CONFIRMACAO_EXCLUSAO_DESCONTO, Constants.MESSAGE_SUCESSO_EXCLUSAO_DESCONTO);
		} else if (item.getItemId()==Menu.FIRST+1){
			try {
				if (valido()){
					alertaMessageConfirmAlteracao(Constants.MESSAGE_CONFIRMACAO_ALTERACAO_DESCONTO, Constants.MESSAGE_CONFIRMACAO_ALTERACAO_DESCONTO);
					
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}else if (item.getItemId()==Menu.FIRST){
			Intent it = getIntent();
			Bundle bundle = it.getExtras();
			boolean consolesAptos = bundle.getBoolean(Constants.SESSION_SEM_CONSOLES_APTOS_DESCONTO);
			item.setVisible(false);
			Spinner seletorConsole = (Spinner)findViewById(R.id.seletor_console);
			EditText text_hora_validade = (EditText)findViewById(R.id.text_hora_validade);
			EditText text_minutos_validade = (EditText) findViewById(R.id.text_minutos_validade);
			EditText text_data_validade = (EditText)findViewById(R.id.text_data_validade);
			EditText text_porcentagem_desconto = (EditText)findViewById(R.id.text_porcentagem_desconto);
			Button button_consoles_participantes = (Button)findViewById(R.id.button_consoles_participantes);
			
			seletorConsole.setEnabled(false);
			text_hora_validade.setEnabled(true);
			text_minutos_validade.setEnabled(true);
			text_data_validade.setEnabled(true);
			text_porcentagem_desconto.setEnabled(true);
			if (consolesAptos){
				button_consoles_participantes.setEnabled(true);
			}else{
				button_consoles_participantes.setEnabled(false);
			}
		}
		
		return true;
	}
	
	public void onButtonClick (View v){
		String listaConsolesSelectAux;
		
		if (TelaSelecaoConsolesParticipantes.lstConsolesSelecionados!=null){
		listaConsolesSelectAux = TelaSelecaoConsolesParticipantes.lstConsolesSelecionados;
		}
		else{
			listaConsolesSelectAux="";
		}
		System.out.println("listaConsolesSelectAux: "+listaConsolesSelectAux);
		
		
		if (R.id.button_consoles_participantes==v.getId()){
			counterConsoles++;
			Intent intent = new Intent(TelaDescontoActivity.this,TelaSelecaoConsolesParticipantes.class);
			Bundle bundle1 = intent.getExtras();
			if (bundle1==null){
				bundle1 = new Bundle();
			}
			bundle1.putString(Constants.SESSION_CONSOLES_PARTICIPANTES, listaConsolesSelectAux);
			bundle1.putInt(Constants.SESSION_NOVA_MANUTENCAO, counterConsoles);
			intent.putExtras(bundle1);
			startActivity(intent);
		}
		
	}
	
	public boolean valido() throws ParseException{
		boolean valido = true;
		EditText text_porcentagem_desconto = (EditText)findViewById(R.id.text_porcentagem_desconto);
		EditText text_data_validade = (EditText)findViewById(R.id.text_data_validade);
		EditText text_hora_validade = (EditText)findViewById(R.id.text_hora_validade);
		EditText text_minutos_validade = (EditText) findViewById(R.id.text_minutos_validade);
		
		if (GamewareUtils.campoVazio(text_porcentagem_desconto)) {
			Toast.makeText(TelaDescontoActivity.this, "Voc� deve preencher o campo Porcentagem de Desconto", Toast.LENGTH_SHORT).show();
			valido = false;
		}
		if (GamewareUtils.campoVazio(text_data_validade)) {
			Toast.makeText(TelaDescontoActivity.this, "Voc� deve preencher o campo Data de Validade", Toast.LENGTH_SHORT).show();
			valido = false;
		}
		if (GamewareUtils.campoVazio(text_hora_validade)) {
			Toast.makeText(TelaDescontoActivity.this, "Voc� deve preencher o campo Horas", Toast.LENGTH_SHORT).show();
			valido = false;
		}
		if (GamewareUtils.campoVazio(text_minutos_validade)) {
			Toast.makeText(TelaDescontoActivity.this, "Voc� deve preencher o campo Minutos", Toast.LENGTH_SHORT).show();
			valido = false;
		}
		
		String porcentagem_texto = text_porcentagem_desconto.getText().toString().trim();
		
		porcentagem_texto = porcentagem_texto.replace("%", Constants.EMPTY);
		
		Double porcentagem = 0.00;
		if (!porcentagem_texto.isEmpty()){
		porcentagem = Double.valueOf(porcentagem_texto);
		}
		
		if (porcentagem<0 || porcentagem>100){
			Toast.makeText(TelaDescontoActivity.this, "Informe um valor v�lido para o campo Porcentagem de Desconto", Toast.LENGTH_SHORT).show();
			valido = false;
		}
		
		if (!GamewareUtils.validaData(text_data_validade.getText().toString())){
			Toast.makeText(TelaDescontoActivity.this, "Informe uma data v�lida", Toast.LENGTH_SHORT).show();
			valido = false;
		}

		if (valido) {
			if (!GamewareUtils.campoVazio(text_data_validade)) {
				if (!GamewareUtils
						.dataMaiorIgualAtual(text_data_validade.getText().toString()
								.substring(0, 2), text_data_validade.getText()
								.toString().substring(3, 5), text_data_validade
								.getText().toString().substring(6, 10),text_hora_validade.getText().toString(),text_minutos_validade.getText().toString())) {
					Toast.makeText(TelaDescontoActivity.this,
							"Informe uma data maior que a atual",
							Toast.LENGTH_SHORT).show();
					valido = false;
				}
			}
		}
		
		if (!GamewareUtils.campoVazio(text_hora_validade)) {
			Integer horas = Integer.valueOf(text_hora_validade.getText()
					.toString());
			if (horas < 0 || horas > 24) {
				Toast.makeText(TelaDescontoActivity.this,
						"Informe um valor v�lido para o campo Horas",
						Toast.LENGTH_SHORT).show();
				valido = false;
			}
		}
		
		if (!GamewareUtils.campoVazio(text_minutos_validade)) {
			Integer horas = Integer.valueOf(text_minutos_validade.getText()
					.toString());
			if (horas < 0 || horas > 59) {
				Toast.makeText(TelaDescontoActivity.this,
						"Informe um valor v�lido para o campo Minutos",
						Toast.LENGTH_SHORT).show();
				valido = false;
			}
		}
		return valido;
	}
	
	public void alertaMessageOk(String mensagem){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// set dialog message
		alertDialogBuilder
			.setMessage(mensagem)
			.setCancelable(false)
			.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					finish();
				}
			  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
	}
	
	public void alertaMessageConfirmAlteracao (String mensagemQuestion, final String mensagemSucess){
		final Spinner seletor_desconto = (Spinner) findViewById(R.id.seletor_desconto);
		
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(mensagemQuestion)
				.setCancelable(false)
				.setPositiveButton(Constants.BUTTON_SIM,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								String mensagemSucessAux = mensagemSucess;
								Integer position = seletor_desconto.getSelectedItemPosition();
								EditText text_porcentagem_desconto = (EditText)findViewById(R.id.text_porcentagem_desconto);
								EditText text_data_validade = (EditText)findViewById(R.id.text_data_validade);
								EditText text_hora_validade = (EditText)findViewById(R.id.text_hora_validade);
								EditText text_minutos_validade = (EditText) findViewById(R.id.text_minutos_validade);
								
								SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMATO_DATA_COMPLETO);
								String data = GamewareUtils.stringEditText(text_data_validade)+" "+GamewareUtils.stringEditText(text_hora_validade)+":"+GamewareUtils.stringEditText(text_minutos_validade);
								
								//TODO: Recuperar os consoles aqui
								List<Console> listConsoleWS = new ArrayList<Console>();
								Desconto descontoAlterar = new Desconto();
								descontoAlterar.setIdDesconto(listConsoleWS.get(position).getIdConsole());
								descontoAlterar.setInicioValidade(new Date());
								try {
									descontoAlterar.setFimValidade(sdf.parse(data));
								} catch (ParseException e) {
									e.printStackTrace();
								}
								descontoAlterar.setPorcentagemDesconto(Integer.valueOf(GamewareUtils.stringEditText(text_porcentagem_desconto)));
								
								boolean sucesso = true;
								if (sucesso){
									
								}else{
									mensagemSucessAux = Constants.MESSAGE_ERRO_BD;
								}
								
								final String mensagemAux2 = mensagemSucessAux;
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TelaDescontoActivity.this);
						 
									// set dialog message
									alertDialogBuilder
										.setMessage(mensagemSucess)
										.setCancelable(false)
										.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) {
												finish();
											}
										  });
						 
										// create alert dialog
										AlertDialog alertDialog = alertDialogBuilder.create();
						 
										// show it
										alertDialog.show();
							}
						})
				.setNegativeButton(Constants.BUTTON_NAO,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	
	}
	
	public void alertaMessageConfirmExclusao (String mensagemQuestion, final String mensagemSucess){

		final Spinner seletor_desconto = (Spinner) findViewById(R.id.seletor_desconto);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(mensagemQuestion)
				.setCancelable(false)
				.setPositiveButton(Constants.BUTTON_SIM,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								String mensagemSucessAux = mensagemSucess;
								Integer position = seletor_desconto.getSelectedItemPosition();
								boolean sucesso = true;
								if (sucesso){
									
								}else{
									mensagemSucessAux = Constants.MESSAGE_ERRO_BD;
								}
								
								final String mensagemAux2 = mensagemSucessAux;
								 //TODO: Chamar o servi�o para exclus�o do desconto
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TelaDescontoActivity.this);
						 
									// set dialog message
									alertDialogBuilder
										.setMessage(mensagemAux2)
										.setCancelable(false)
										.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) {
												finish();
											}
										  });
						 
										// create alert dialog
										AlertDialog alertDialog = alertDialogBuilder.create();
						 
										// show it
										alertDialog.show();
							}
						})
				.setNegativeButton(Constants.BUTTON_NAO,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	
	}
	
//	@Override
//	public void onResume() {
//		super.onResume();
//		Intent it = getIntent();
//		Bundle bundle = it.getExtras();
//		opcaoManutencaoDesconto = bundle.getInt(Constants.SESSION_MANUTENCAO_DESCONTO);
//		
//		if (opcaoManutencaoDesconto == Constants.ALTER_CONSOLE) {
//			setContentView(R.layout.activity_tela_dados_cadastrais_desconto);
//		} else if (opcaoManutencaoDesconto == Constants.CADASTRO_CONSOLE) {
//			setContentView(R.layout.activity_tela_desconto_cadastro);
//		}
//	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		super.finish();
	}
	
}