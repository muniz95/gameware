package com.gameware.tcc2;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gameware.tcc2.model.Console;
import com.gameware.tcc2.utils.CelulaREST;
import com.gameware.tcc2.utils.Constants;
import com.gameware.tcc2.utils.GamewareUtils;
import com.gameware.tcc2.utils.MaskUtils;

public class TelaCadastroDescontoActivity extends Activity {

	private TextWatcher dtValidadeMask;
	Integer counterConsoles;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_tela_desconto_cadastro);
		counterConsoles=0;
		Button buttonConsoles = (Button) findViewById(R.id.button_consoles_participantes);
		EditText dtValidade = (EditText)findViewById(R.id.text_data_validade);
		final EditText porcentagem = (EditText)findViewById(R.id.text_porcentagem_desconto);
		porcentagem.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus){
					if (!GamewareUtils.campoVazio(porcentagem)){
						if (!porcentagem.getText().toString().contains("%")){
						porcentagem.setText(porcentagem.getText()+"%");
						}
					}
				}
				
			}
		});
		dtValidadeMask = MaskUtils.insert(Constants.MASK_FORMAT_DATA,dtValidade);
		dtValidade.addTextChangedListener(dtValidadeMask);
		
		List<Console> consoleList = new ArrayList<Console>();
		try {
			consoleList = CelulaREST.listaConsolesAptosDesconto();
			if (consoleList.size()==0){
				buttonConsoles.setEnabled(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void onButtonClick (View v) throws ParseException{
		String listaConsolesSelectAux;
		
		
		
		if (TelaSelecaoConsolesParticipantes.lstConsolesSelecionados!=null){
		listaConsolesSelectAux = TelaSelecaoConsolesParticipantes.lstConsolesSelecionados;
		}
		else{
			listaConsolesSelectAux="";
		}
		
		/*Criar valida��o para entrar na lista de consoles participantes apenas quando houver pelo menos 1 console apto ao desconto*/
		
		if (R.id.button_consoles_participantes==v.getId()){
			counterConsoles++;
			Intent intent = new Intent(TelaCadastroDescontoActivity.this,TelaSelecaoConsolesParticipantes.class);
			Bundle bundle1 = intent.getExtras();
			if (bundle1==null){
				bundle1 = new Bundle();
			}
			bundle1.putString(Constants.SESSION_CONSOLES_PARTICIPANTES, listaConsolesSelectAux);
			bundle1.putInt(Constants.SESSION_NOVA_MANUTENCAO, counterConsoles);
			intent.putExtras(bundle1);
			startActivity(intent);
		}
		
		if (R.id.button_ok==v.getId()){
			if (valido()){
				alertaMessageOk(Constants.MESSAGE_SUCESSO_CADASTRO_DESCONTO);
			}
		}
			
			
			
			
		/*Consoles selecionados na lista*/
		if (TelaSelecaoConsolesParticipantes.lstConsolesSelecionados!=null){
			System.out.println(TelaSelecaoConsolesParticipantes.lstConsolesSelecionados);
			}
		
	}
	
	public void alertaMessageOk(String mensagem){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// set dialog message
		alertDialogBuilder
			.setMessage(mensagem)
			.setCancelable(false)
			.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					finish();
				}
			  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
	}

	public boolean valido() throws ParseException{
		boolean valido = true;
		EditText text_tipo_desconto = (EditText)findViewById(R.id.text_tipo_desconto);
		EditText text_porcentagem_desconto = (EditText)findViewById(R.id.text_porcentagem_desconto);
		EditText text_data_validade = (EditText)findViewById(R.id.text_data_validade);
		EditText text_hora_validade = (EditText)findViewById(R.id.text_hora_validade);
		EditText text_minutos_validade = (EditText) findViewById(R.id.text_minutos_validade);
		
		if (GamewareUtils.campoVazio(text_tipo_desconto)) {
			Toast.makeText(TelaCadastroDescontoActivity.this, "Voc� deve preencher o campo Tipo de Desconto", Toast.LENGTH_SHORT).show();
			valido = false;
		}
		if (GamewareUtils.campoVazio(text_porcentagem_desconto)) {
			Toast.makeText(TelaCadastroDescontoActivity.this, "Voc� deve preencher o campo Porcentagem de Desconto", Toast.LENGTH_SHORT).show();
			valido = false;
		}
		if (GamewareUtils.campoVazio(text_data_validade)) {
			Toast.makeText(TelaCadastroDescontoActivity.this, "Voc� deve preencher o campo Data de Validade", Toast.LENGTH_SHORT).show();
			valido = false;
		}
		if (GamewareUtils.campoVazio(text_hora_validade)) {
			Toast.makeText(TelaCadastroDescontoActivity.this, "Voc� deve preencher o campo Horas", Toast.LENGTH_SHORT).show();
			valido = false;
		}
		if (GamewareUtils.campoVazio(text_minutos_validade)) {
			Toast.makeText(TelaCadastroDescontoActivity.this, "Voc� deve preencher o campo Minutos", Toast.LENGTH_SHORT).show();
			valido = false;
		}
		
		String porcentagem_texto = text_porcentagem_desconto.getText().toString().trim();
		
		porcentagem_texto = porcentagem_texto.replace("%", Constants.EMPTY);
		
		Double porcentagem = 0.00;
		if (!porcentagem_texto.isEmpty()){
		porcentagem = Double.valueOf(porcentagem_texto);
		}
		
		if (porcentagem<0 || porcentagem>100){
			Toast.makeText(TelaCadastroDescontoActivity.this, "Informe um valor v�lido para o campo Porcentagem de Desconto", Toast.LENGTH_SHORT).show();
			valido = false;
		}
		
		if (!GamewareUtils.validaData(text_data_validade.getText().toString())){
			Toast.makeText(TelaCadastroDescontoActivity.this, "Informe uma data v�lida", Toast.LENGTH_SHORT).show();
			valido = false;
		}

		if (valido) {
			if (!GamewareUtils.campoVazio(text_data_validade)) {
				if (!GamewareUtils
						.dataMaiorIgualAtual(text_data_validade.getText().toString()
								.substring(0, 2), text_data_validade.getText()
								.toString().substring(3, 5), text_data_validade
								.getText().toString().substring(6, 10),text_hora_validade.getText().toString(),text_minutos_validade.getText().toString())) {
					Toast.makeText(TelaCadastroDescontoActivity.this,
							"Informe uma data maior que a atual",
							Toast.LENGTH_SHORT).show();
					valido = false;
				}
			}
		}
		
		if (!GamewareUtils.campoVazio(text_hora_validade)) {
			Integer horas = Integer.valueOf(text_hora_validade.getText()
					.toString());
			if (horas < 0 || horas > 24) {
				Toast.makeText(TelaCadastroDescontoActivity.this,
						"Informe um valor v�lido para o campo Horas",
						Toast.LENGTH_SHORT).show();
				valido = false;
			}
		}
		
		if (!GamewareUtils.campoVazio(text_minutos_validade)) {
			Integer horas = Integer.valueOf(text_minutos_validade.getText()
					.toString());
			if (horas < 0 || horas > 59) {
				Toast.makeText(TelaCadastroDescontoActivity.this,
						"Informe um valor v�lido para o campo Minutos",
						Toast.LENGTH_SHORT).show();
				valido = false;
			}
		}
		return valido;
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		super.finish();
	}
	
}