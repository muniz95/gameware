package com.gameware.tcc2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

import com.gameware.tcc2.model.RendimentosCliente;
import com.gameware.tcc2.utils.CelulaREST;
import com.gameware.tcc2.utils.Constants;
import com.gameware.tcc2.utils.GamewareUtils;

public class TelaRendimentosClienteActivity extends ListActivity{

	public static final String TAG_CLIENT_NAME = "clientName";
	public static final String TAG_CLIENT_REND = "clientRend";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_rendimentos_cliente);
		ArrayList<HashMap<String, String>> consoleList = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map = new HashMap<String, String>();
		
		Intent it = getIntent();
		Bundle bundle = it.getExtras();
		
		List<RendimentosCliente> rendimentosList = new ArrayList<RendimentosCliente>();
		try {
			rendimentosList = CelulaREST.retornaRendimentosCliente(bundle.getString(Constants.SESSION_DATE_QUERY), bundle.getString(Constants.SESSION_DATE_QUERY1));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		for (Iterator iterator = rendimentosList.iterator(); iterator.hasNext();) {
			RendimentosCliente rendimentosCliente = (RendimentosCliente) iterator.next();
			map = new HashMap<String, String>();
			map.put(TAG_CLIENT_NAME, rendimentosCliente.getNomeCliente());
			map.put(TAG_CLIENT_REND, "R$"+GamewareUtils.formatValueDecimal(String.valueOf(rendimentosCliente.getValorGasto()*10)));
			consoleList.add(map);
		}
		
		
		ListAdapter adapter = new SimpleAdapter(this, consoleList,
				R.layout.activity_tela_rendimentos_cliente_item,
				new String[] { TAG_CLIENT_NAME, TAG_CLIENT_REND}, new int[] {
				R.id.clientName, R.id.clientRend});
		
		setListAdapter(adapter);		
		
	}
}