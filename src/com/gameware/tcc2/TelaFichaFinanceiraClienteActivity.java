package com.gameware.tcc2;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

import com.gameware.tcc2.model.FichaCliente;
import com.gameware.tcc2.utils.CelulaREST;
import com.gameware.tcc2.utils.Constants;
import com.gameware.tcc2.utils.GamewareUtils;

public class TelaFichaFinanceiraClienteActivity extends ListActivity {

	public static final String TAG_FINANC_DATE = "financDate";
	public static final String TAG_CONSOLE_NAME = "consoleName";
	public static final String TAG_CONSOLE_REND = "consoleRend";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_ficha_financeira_cliente);
		
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMATO_DATA_COMPLETO);
		
		ArrayList<HashMap<String, String>> consoleList = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map = new HashMap<String, String>();
		//TODO: Dados do cliente
		Intent it = getIntent();
		Bundle bundle = it.getExtras();
		Integer indiceSelecionado = bundle.getInt(Constants.SESSION_INDICE_SELECIONADO);
		
		//TODO: Chamada do webservice aqui - ordens de servico
		List<FichaCliente> locacaoList = new ArrayList<FichaCliente>();
		try {
			locacaoList = CelulaREST.retornaFichaCliente(indiceSelecionado);
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (Iterator iterator = locacaoList.iterator(); iterator.hasNext();) {
			FichaCliente locacao = (FichaCliente) iterator.next();
			map = new HashMap<String, String>();
			map.put(TAG_FINANC_DATE, sdf.format(locacao.getInicioLocacao())+" a "+sdf.format(locacao.getFimLocacao()));
			map.put(TAG_CONSOLE_NAME, locacao.getNomeConsole());
			map.put(TAG_CONSOLE_REND, "R$"+GamewareUtils.formatValueDecimal(String.valueOf(locacao.getValorGasto()*10)));
			consoleList.add(map);
		}
			
		ListAdapter adapter = new SimpleAdapter(this, consoleList,
				R.layout.activity_tela_ficha_financeira_cliente_item,
				new String[] { TAG_FINANC_DATE, TAG_CONSOLE_NAME, TAG_CONSOLE_REND}, new int[] {
				R.id.financDate, R.id.consoleName, R.id.consoleRend});
		
		setListAdapter(adapter);
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		super.finish();
	}
	
}