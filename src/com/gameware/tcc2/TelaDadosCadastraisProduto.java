package com.gameware.tcc2;

import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gameware.tcc2.model.FichaProduto;
import com.gameware.tcc2.model.Status;
import com.gameware.tcc2.utils.CelulaREST;
import com.gameware.tcc2.utils.Constants;
import com.gameware.tcc2.utils.GamewareUtils;

public class TelaDadosCadastraisProduto extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_dados_cadastrais_produto);
		
		FichaProduto produto = new FichaProduto();
		EditText textNomeProduto = (EditText) findViewById(R.id.text_nome_produto);
		TextView seletorCategoria = (TextView) findViewById(R.id.seletor_categoria);
		TextView seletorConsoleAssociado = (TextView) findViewById(R.id.seletor_console_associado);
		EditText textQuantidadeEstoque = (EditText) findViewById(R.id.text_qtde_estoque);
		EditText textValor = (EditText) findViewById(R.id.text_valor);
		TextView label_valor = (TextView) findViewById(R.id.label_valor);
		TextView labelQuantidadeEstoque = (TextView)findViewById(R.id.label_qtde_estoque);
		
		textNomeProduto.setEnabled(false);
		seletorCategoria.setEnabled(false);
		seletorConsoleAssociado.setEnabled(false);
		textQuantidadeEstoque.setEnabled(false);
		textValor.setEnabled(false);
		
		Intent it = getIntent();
		Bundle bundle = it.getExtras();
		Integer indiceSelecionado = bundle.getInt(Constants.SESSION_INDICE_SELECIONADO);

		
		try {
			produto = CelulaREST.retornaProduto(indiceSelecionado);
		} catch (Exception e) {
			alertaMessageErro(Constants.MESSAGE_ERRO_BD);
		}
		
		textNomeProduto.setText(produto.getNomeProduto());
		textQuantidadeEstoque.setText(String.valueOf(produto.getQuantidadeEstoque()));
		textValor.setText(String.valueOf(produto.getValorMonetario()));
		seletorCategoria.setText(produto.getNomeTipo());
		seletorConsoleAssociado.setText(produto.getNomeConsole());
		if (produto.isAssociadoConsole()){
			label_valor.setVisibility(View.GONE);
			textValor.setVisibility(View.GONE);
			textQuantidadeEstoque.setVisibility(View.GONE);
			labelQuantidadeEstoque.setVisibility(View.GONE);
		}
		
		bundle.putBoolean(Constants.SESSION_ASSOCIADO_CONSOLE,produto.isAssociadoConsole());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(Menu.CATEGORY_ALTERNATIVE, Menu.FIRST, Menu.NONE, Constants.MENU_EDITAR);
		menu.add(Menu.NONE, Menu.FIRST+1, Menu.NONE, Constants.MENU_SALVAR);
		menu.add(Menu.NONE, Menu.FIRST+2, Menu.NONE, Constants.MENU_EXCLUIR_PRODUTO);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		if (item.getItemId()==Menu.FIRST+2){
			alertaMessageConfirmExclusao(Constants.MESSAGE_CONFIRMACAO_EXCLUSAO_PRODUTO, Constants.MESSAGE_SUCESSO_EXCLUSAO_PRODUTO);
		} else if (item.getItemId()==Menu.FIRST+1){
			if (this.valido()){
				alertaMessageConfirmAlteracao(Constants.MESSAGE_CONFIRMACAO_ALTERACAO_PRODUTO, Constants.MESSAGE_SUCESSO_ALTERACAO_PRODUTO);
			}	
		}else if (item.getItemId()==Menu.FIRST){
			item.setVisible(false);

			EditText textNomeProduto = (EditText) findViewById(R.id.text_nome_produto);
			EditText textQuantidadeEstoque = (EditText) findViewById(R.id.text_qtde_estoque);
			EditText textValor = (EditText) findViewById(R.id.text_valor);
			
			textNomeProduto.setEnabled(true);
			textQuantidadeEstoque.setEnabled(true);
			textValor.setEnabled(true);
		}
		
		return true;
	}
	
	public void onButtonClick(View v) {
	}
	
	public void alertaMessageOk(String mensagem){
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// set dialog message
		alertDialogBuilder
			.setMessage(mensagem)
			.setCancelable(false)
			.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					finish();
				}
			  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
	}
	
	public void alertaMessageConfirmAlteracao (String mensagemQuestion, final String mensagemSucess){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(mensagemQuestion)
				.setCancelable(false)
				.setPositiveButton(Constants.BUTTON_SIM,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								Intent it = getIntent();
								Bundle bundle = it.getExtras();
								Integer indiceSelecionado = bundle.getInt(Constants.SESSION_INDICE_SELECIONADO);
								Boolean isConsole = bundle.getBoolean(Constants.SESSION_ASSOCIADO_CONSOLE);
								
								String mensagemSucessAux = mensagemSucess;
								FichaProduto fichaProduto = new FichaProduto();
								
								EditText textNomeProduto = (EditText) findViewById(R.id.text_nome_produto);
								EditText textQuantidadeEstoque = (EditText) findViewById(R.id.text_qtde_estoque);
								EditText textValor = (EditText) findViewById(R.id.text_valor);
								TextView seletorCategoria = (TextView) findViewById(R.id.seletor_categoria);
								TextView seletorConsoleAssociado = (TextView) findViewById(R.id.seletor_console_associado);
								
								
								fichaProduto.setNomeProduto(GamewareUtils.stringEditText(textNomeProduto));
								
								fichaProduto.setQuantidadeEstoque(Integer.valueOf(GamewareUtils.stringEditText(textQuantidadeEstoque)));
								fichaProduto.setValorMonetario(Float.valueOf(GamewareUtils.stringEditText(textValor)));
								
								Status status = new Status();
								
								try {
									status = CelulaREST.alteraProduto(indiceSelecionado, fichaProduto.getNomeProduto(), fichaProduto.getQuantidadeEstoque(), fichaProduto.getValorMonetario(), isConsole);
								} catch (JSONException e) {
									status.setCodigoErro(Constants.ERRO_OPERACAO_BD);
									e.printStackTrace();
								}
								
								if (Constants.SUCESSO_OPERACAO_BD.equals(status.getCodigoErro())){
									
								}else if (Constants.ERRO_OPERACAO_BD.equals(status.getCodigoErro())){
									mensagemSucessAux = Constants.MESSAGE_ERRO_BD;
									alertaMessageErro(mensagemSucessAux);
								}else if (Constants.ERRO_PRODUTO_EXISTENTE.equals(status.getCodigoErro())){
									mensagemSucessAux = Constants.MESSAGE_ERRO_CADASTRO_PRODUTO;
									alertaMessageErro(mensagemSucessAux);
								}
								
								final String mensagemAux2 = mensagemSucessAux;
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TelaDadosCadastraisProduto.this);
						 
									// set dialog message
									alertDialogBuilder
										.setMessage(mensagemAux2)
										.setCancelable(false)
										.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) {
												finish();
											}
										  });
						 
										// create alert dialog
										AlertDialog alertDialog = alertDialogBuilder.create();
						 
										// show it
										alertDialog.show();
							}
						})
				.setNegativeButton(Constants.BUTTON_NAO,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	
	}
	
	public void alertaMessageConfirmExclusao (String mensagemQuestion, final String mensagemSucess){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(mensagemQuestion)
				.setCancelable(false)
				.setPositiveButton(Constants.BUTTON_SIM,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								String mensagemSucessAux = mensagemSucess;
								
								//TODO: Realizar a exclusao com o indice selecionado
								Intent it = getIntent();
								Bundle bundle = it.getExtras();
								Integer indiceSelecionado = bundle.getInt(Constants.SESSION_INDICE_SELECIONADO);
								boolean sucesso = true;
								if (sucesso){
									
								}else{
									mensagemSucessAux = Constants.MESSAGE_ERRO_BD;
								}
								
								final String mensagemAux2 = mensagemSucessAux;
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TelaDadosCadastraisProduto.this);
						 
									// set dialog message
									alertDialogBuilder
										.setMessage(mensagemSucess)
										.setCancelable(false)
										.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) {
												finish();
											}
										  });
						 
										// create alert dialog
										AlertDialog alertDialog = alertDialogBuilder.create();
						 
										// show it
										alertDialog.show();
							}
						})
				.setNegativeButton(Constants.BUTTON_NAO,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	
	}
	
	public boolean valido(){

		EditText textNomeProduto = (EditText) findViewById(R.id.text_nome_produto);
		EditText textQuantidadeEstoque = (EditText) findViewById(R.id.text_qtde_estoque);
		EditText textValor = (EditText) findViewById(R.id.text_valor);
		boolean valido = true;
		
		if (GamewareUtils.campoVazio(textNomeProduto)){
			Toast.makeText(TelaDadosCadastraisProduto.this, "Voc� deve preencher o campo Nome do Produto", Toast.LENGTH_SHORT).show();
			valido = false;
		}
		if (GamewareUtils.campoVazio(textQuantidadeEstoque)){
			Toast.makeText(TelaDadosCadastraisProduto.this, "Voc� deve preencher o campo Quantidade no Estoque", Toast.LENGTH_SHORT).show();
			valido = false;
		}
		if (GamewareUtils.campoVazio(textValor)){
			Toast.makeText(TelaDadosCadastraisProduto.this, "Voc� deve preencher o campo Valor", Toast.LENGTH_SHORT).show();
			valido = false;
		}
		
		return valido;
	
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		super.finish();
	}
	
	public void alertaMessageErro(String mensagem){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// set dialog message
		alertDialogBuilder
			.setMessage(mensagem)
			.setCancelable(false)
			.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.cancel();
				}
			  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
	}
}
