package com.gameware.tcc2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.gameware.tcc2.model.Locacao;
import com.gameware.tcc2.utils.CelulaREST;
import com.gameware.tcc2.utils.Constants;
import com.gameware.tcc2.utils.GamewareUtils;

public class TelaFaturamentoPeriodoActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_faturamento_periodo);
		Intent it = getIntent();
		Bundle bundle = it.getExtras();
		Integer periodOption = bundle.getInt(Constants.SESSION_PERIOD_OPTION);
		String dataConsulta = bundle.getString(Constants.SESSION_DATE_QUERY);
		String dataConsulta1 = bundle.getString(Constants.SESSION_DATE_QUERY1);
		Double totalFaturamento = bundle.getDouble(Constants.SESSION_RENDIMENTO_TOTAL_PERIODO);
		TextView periodoInformado = (TextView) findViewById(R.id.text_periodo_infomado);
		TextView totalPeriodo = (TextView) findViewById(R.id.text_valor);

		Locacao locacao = new Locacao();
		try {
			locacao = CelulaREST.retornaFaturamentoTotal(dataConsulta, dataConsulta1);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		
		totalPeriodo.setText("R$"+GamewareUtils.formatValueDecimal(String.valueOf(locacao.getValorGasto()*10)));
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdfMes = new SimpleDateFormat("MM/yyyy");
		SimpleDateFormat sdfAno = new SimpleDateFormat("yyyy");
		
		if (periodOption==Constants.PERIODO_DIA){
			periodoInformado.setText("Per�odo informado: "+dataConsulta);
		}else if (periodOption==Constants.PERIODO_MES){
			Date dataAux = new Date();
			try {
				dataAux = sdf.parse(dataConsulta);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			periodoInformado.setText("Per�odo informado: "+sdfMes.format(dataAux));
		}else if (periodOption==Constants.PERIODO_ANO){
			Date dataAux = new Date();
			try {
				dataAux = sdf.parse(dataConsulta);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			periodoInformado.setText("Per�odo informado: "+sdfAno.format(dataAux));
		}else if (periodOption==Constants.PERIODO_PER){
			periodoInformado.setText("Per�odo informado: "+dataConsulta+" a "+dataConsulta1);
		}
	}

	public void onButtonClick (View v){
		if (R.id.button_rendimentos_cliente==v.getId()){
			Intent intent = new Intent(TelaFaturamentoPeriodoActivity.this,TelaRendimentosClienteActivity.class);
			Intent it = getIntent();
			intent.putExtras(it.getExtras());
			startActivity(intent);			
		}else if (R.id.button_rendimentos_console==v.getId()){
			Intent intent = new Intent(TelaFaturamentoPeriodoActivity.this,TelaRendimentosConsoleActivity.class);
			Intent it = getIntent();
			intent.putExtras(it.getExtras());
			startActivity(intent);					
		}
	}
}