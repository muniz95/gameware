package com.gameware.tcc2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.gameware.tcc2.model.CadastroCliente;
import com.gameware.tcc2.model.Status;
import com.gameware.tcc2.utils.CelulaREST;
import com.gameware.tcc2.utils.Constants;
import com.gameware.tcc2.utils.GamewareUtils;
import com.gameware.tcc2.utils.MaskUtils;

public class TelaDadosCadastraisClienteActivity extends Activity {

	String sexoCliente;
	private TextWatcher cpfMask;
	private TextWatcher rgMask;
	private TextWatcher dtNascMask;
	private TextWatcher telResMask;
	private TextWatcher telCelMask;
	private TextWatcher cepMask;
	private TextWatcher whatsappMask;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_dados_cadastrais_cliente);
		
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMATO_DATA);
		
		Spinner campoEstado = (Spinner) findViewById(R.id.seletor_estado);
		
		List<String> listaEstado = new ArrayList<String>();
		listaEstado.add("AC");
		listaEstado.add("AL");
		listaEstado.add("AM");
		listaEstado.add("AP");
		listaEstado.add("BA");
		listaEstado.add("CE");
		listaEstado.add("DF");
		listaEstado.add("ES");
		listaEstado.add("GO");
		listaEstado.add("MA");
		listaEstado.add("MT");
		listaEstado.add("MS");
		listaEstado.add("MG");
		listaEstado.add("PA");
		listaEstado.add("PB");
		listaEstado.add("PE");
		listaEstado.add("PR");
		listaEstado.add("PI");
		listaEstado.add("RJ");
		listaEstado.add("RN");
		listaEstado.add("RO");
		listaEstado.add("RR");
		listaEstado.add("RS");
		listaEstado.add("SE");
		listaEstado.add("SC");
		listaEstado.add("SP");
		listaEstado.add("TO");
		ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, listaEstado);
		campoEstado.setAdapter(adapter);
		campoEstado.setEnabled(false);
		
		EditText textFieldNomeCompleto = (EditText) findViewById(R.id.text_nome_completo);
		CheckBox checkBoxSexoM = (CheckBox) findViewById(R.id.checkBox_sexo_M);
		CheckBox checkBoxSexoF = (CheckBox) findViewById(R.id.checkBox_sexo_F);
		EditText textFieldDataNascimento = (EditText) findViewById(R.id.text_data_nascimento);
		EditText textFieldRg = (EditText) findViewById(R.id.text_rg_cliente);
		EditText textFieldEmail = (EditText) findViewById(R.id.text_email);
		EditText textFieldTelefoneRes = (EditText) findViewById(R.id.text_telefone_res);
		EditText textFieldTelefoneCel = (EditText) findViewById(R.id.text_telefone_cel);
		EditText textFieldCpf = (EditText) findViewById(R.id.text_cpf);
		EditText textFieldSkype = (EditText) findViewById(R.id.text_skype);
		EditText campoEndRua = (EditText) findViewById(R.id.text_end_rua);
		EditText campoEndNum = (EditText) findViewById(R.id.text_end_num);
		EditText campoEndBairro = (EditText) findViewById(R.id.text_end_bairro);
		EditText campoEndCidade = (EditText) findViewById(R.id.text_end_cidade);
		EditText campoEndCep = (EditText) findViewById(R.id.text_end_cep);
		EditText campoFacebook = (EditText) findViewById(R.id.text_facebook);
		EditText campoGtalk = (EditText) findViewById(R.id.text_gtalk);
		EditText campoWhatsApp = (EditText) findViewById(R.id.text_whatsapp);
		EditText campoEndCompl = (EditText) findViewById(R.id.text_end_compl);
		
		textFieldNomeCompleto.setEnabled(false);
		checkBoxSexoM.setEnabled(false);
		checkBoxSexoF.setEnabled(false);
		textFieldDataNascimento.setEnabled(false);
		textFieldRg.setEnabled(false);
		textFieldTelefoneRes.setEnabled(false);
		textFieldTelefoneCel.setEnabled(false);
		textFieldEmail.setEnabled(false);
		textFieldCpf.setEnabled(false);
		textFieldSkype.setEnabled(false);
		campoEndRua.setEnabled(false);
		campoEndNum.setEnabled(false);
		campoEndBairro.setEnabled(false);
		campoEndCidade.setEnabled(false);
		campoEndCep.setEnabled(false);
		campoFacebook.setEnabled(false);
		campoGtalk.setEnabled(false);
		campoWhatsApp.setEnabled(false);
		campoEndCompl.setEnabled(false);
		
		//TODO: Dados do cliente
		Intent it = getIntent();
		Bundle bundle = it.getExtras();
		Integer indiceSelecionado = bundle.getInt(Constants.SESSION_INDICE_SELECIONADO);
		//Mesma pesquisa da tela anterior para pegar os dados baseados no indice selecionado
		CadastroCliente dadosUsuario = new CadastroCliente();
		try {
			dadosUsuario = CelulaREST.retornaCliente(indiceSelecionado);
		} catch (Exception e) {
			e.printStackTrace();
		}
		textFieldNomeCompleto.setText(dadosUsuario.getNome());
		if (Constants.CLIENTE_SEXO_F==sexoCliente){
			checkBoxSexoF.setChecked(true);
		}else{
			checkBoxSexoM.setChecked(true);
		}
		checkBoxSexoM.setEnabled(false);
		checkBoxSexoF.setEnabled(false);
		sexoCliente = dadosUsuario.getSexo();
		textFieldDataNascimento.setText(sdf.format(dadosUsuario.getDataNascimento()));
		textFieldTelefoneRes.setText(dadosUsuario.getTelefone());
		textFieldTelefoneCel.setText(dadosUsuario.getTelefone_cel());
		textFieldRg.setText(String.valueOf(dadosUsuario.getRg()));
		textFieldEmail.setText(dadosUsuario.getEmail());
		textFieldCpf.setText(String.valueOf(dadosUsuario.getCpf()));
		textFieldSkype.setText(dadosUsuario.getSkype());
		campoEndRua.setText(dadosUsuario.getEndRua());
		campoEndNum.setText(dadosUsuario.getEndNum());
		campoEndBairro.setText(dadosUsuario.getEndBairro());
		campoEndCidade.setText(dadosUsuario.getEndCidade());
		campoEndCep.setText(dadosUsuario.getEndCep());
		campoFacebook.setText(dadosUsuario.getFacebook());
		campoGtalk.setText(dadosUsuario.getGtalk());
		campoWhatsApp.setText(dadosUsuario.getWhatsapp());
		campoEndCompl.setText(dadosUsuario.getEndCompl());
		
		int position = 0;
		for (Iterator iterator = listaEstado.iterator(); iterator.hasNext();) {
			String estado = (String) iterator.next();
			if (estado.equals(dadosUsuario.getEndEstado())){
				campoEstado.setSelection(position);
				break;
			}
			position++;
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(Menu.CATEGORY_ALTERNATIVE, Menu.FIRST, Menu.NONE, Constants.MENU_EDITAR);
		menu.add(Menu.NONE, Menu.FIRST+1, Menu.NONE, Constants.MENU_SALVAR);
		menu.add(Menu.NONE, Menu.FIRST+2, Menu.NONE, Constants.MENU_EXCLUIR_CLIENTE);
		menu.setGroupCheckable(Menu.CATEGORY_ALTERNATIVE, false, false);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMATO_DATA);
		EditText textFieldNomeCompleto = (EditText) findViewById(R.id.text_nome_completo);
		CheckBox checkBoxSexoM = (CheckBox) findViewById(R.id.checkBox_sexo_M);
		CheckBox checkBoxSexoF = (CheckBox) findViewById(R.id.checkBox_sexo_F);
		EditText textFieldDataNascimento = (EditText) findViewById(R.id.text_data_nascimento);
		EditText textFieldRg = (EditText) findViewById(R.id.text_rg_cliente);
		EditText textFieldEmail = (EditText) findViewById(R.id.text_email);
		EditText textFieldTelefoneRes = (EditText) findViewById(R.id.text_telefone_res);
		EditText textFieldTelefoneCel = (EditText) findViewById(R.id.text_telefone_cel);
		EditText textFieldCpf = (EditText) findViewById(R.id.text_cpf);
		EditText textFieldSkype = (EditText) findViewById(R.id.text_skype);
		EditText campoEndRua = (EditText) findViewById(R.id.text_end_rua);
		EditText campoEndNum = (EditText) findViewById(R.id.text_end_num);
		EditText campoEndBairro = (EditText) findViewById(R.id.text_end_bairro);
		EditText campoEndCidade = (EditText) findViewById(R.id.text_end_cidade);
		EditText campoEndCep = (EditText) findViewById(R.id.text_end_cep);
		EditText campoFacebook = (EditText) findViewById(R.id.text_facebook);
		EditText campoGtalk = (EditText) findViewById(R.id.text_gtalk);
		EditText campoWhatsApp = (EditText) findViewById(R.id.text_whatsapp);
		EditText campoEndCompl = (EditText) findViewById(R.id.text_end_compl);
		Spinner campoEstado = (Spinner) findViewById(R.id.seletor_estado);
		
		cpfMask = MaskUtils.insert(Constants.MASK_FORMAT_CPF, textFieldCpf);
		textFieldCpf.addTextChangedListener(cpfMask);
		rgMask = MaskUtils.insert(Constants.MASK_FORMAT_RG, textFieldRg);
		textFieldRg.addTextChangedListener(rgMask);
		dtNascMask = MaskUtils.insert(Constants.MASK_FORMAT_DATA, textFieldDataNascimento);
		textFieldDataNascimento.addTextChangedListener(dtNascMask);
		telResMask = MaskUtils.insert(Constants.MASK_FORMAT_TELEFONE, textFieldTelefoneRes);
		textFieldTelefoneRes.addTextChangedListener(telResMask);
		telCelMask = MaskUtils.insert(Constants.MASK_FORMAT_TELEFONE, textFieldTelefoneRes);
		textFieldTelefoneRes.addTextChangedListener(telCelMask);
		cepMask = MaskUtils.insert(Constants.MASK_FORMAT_CEP, campoEndCep);
		campoEndCep.addTextChangedListener(cepMask);
		whatsappMask = MaskUtils.insert(Constants.MASK_FORMAT_TELEFONE, campoWhatsApp);
		campoWhatsApp.addTextChangedListener(whatsappMask);
		
		if (item.getItemId()==Menu.FIRST+2){
			alertaMessageConfirmExclusao(Constants.MESSAGE_CONFIRMACAO_EXCLUSAO_CLIENTE, Constants.MESSAGE_SUCESSO_EXCLUSAO_CLIENTE);
		} else if (item.getItemId()==Menu.FIRST+1){
			if (valido()){
				alertaMessageConfirmAlteracao(Constants.MESSAGE_CONFIRMACAO_ALTERACAO_CLIENTE, Constants.MESSAGE_SUCESSO_ALTERACAO_CLIENTE);
			}
		}else if (item.getItemId()==Menu.FIRST){
			item.setVisible(false);
			
			textFieldNomeCompleto.setEnabled(true);
			if (Constants.CLIENTE_SEXO_F==sexoCliente){
				checkBoxSexoF.setChecked(true);
			}else{
				checkBoxSexoM.setChecked(true);
			}
			checkBoxSexoF.setEnabled(true);
			checkBoxSexoM.setEnabled(true);
			textFieldDataNascimento.setEnabled(true);
			textFieldRg.setEnabled(true);
			textFieldTelefoneRes.setEnabled(true);
			textFieldTelefoneCel.setEnabled(true);
			textFieldEmail.setEnabled(true);
			textFieldCpf.setEnabled(true);
			textFieldSkype.setEnabled(true);
			campoEndRua.setEnabled(true);
			campoEndNum.setEnabled(true);
			campoEndBairro.setEnabled(true);
			campoEndCidade.setEnabled(true);
			campoEndCep.setEnabled(true);
			campoFacebook.setEnabled(true);
			campoGtalk.setEnabled(true);
			campoWhatsApp.setEnabled(true);
			campoEndCompl.setEnabled(true);
			campoEstado.setEnabled(true);
		}
		
		return true;
	}
	
	public void onButtonClick(View v) {

		CheckBox checkBoxSexoM = (CheckBox) findViewById(R.id.checkBox_sexo_M);
		CheckBox checkBoxSexoF = (CheckBox) findViewById(R.id.checkBox_sexo_F);

		if (R.id.checkBox_sexo_F == v.getId()) {
			if (checkBoxSexoF.isChecked()) {
				checkBoxSexoM.setChecked(false);
			} else if (!checkBoxSexoF.isChecked()) {
				checkBoxSexoM.setChecked(true);
			}
		} else if (R.id.checkBox_sexo_M == v.getId()) {
			if (checkBoxSexoM.isChecked()) {
				checkBoxSexoF.setChecked(false);
			} else if (!checkBoxSexoM.isChecked()) {
				checkBoxSexoF.setChecked(true);
			}
		}
	}
	
	public boolean valido(){

		CheckBox checkBoxSexoF = (CheckBox) findViewById(R.id.checkBox_sexo_F);
		CheckBox checkBoxSexoM = (CheckBox) findViewById(R.id.checkBox_sexo_M);
		EditText campoNome = (EditText) findViewById(R.id.text_nome_completo);
		EditText campoDataNascimento = (EditText) findViewById(R.id.text_data_nascimento);
		EditText campoEmail = (EditText) findViewById(R.id.text_email);
		EditText campoTelefoneRes = (EditText) findViewById(R.id.text_telefone_res);
		EditText campoTelefoneCel = (EditText) findViewById(R.id.text_telefone_cel);
		EditText campoEndRua = (EditText) findViewById(R.id.text_end_rua);
		EditText campoEndNum = (EditText) findViewById(R.id.text_end_num);
		EditText campoEndBairro = (EditText) findViewById(R.id.text_end_bairro);
		EditText campoEndCidade = (EditText) findViewById(R.id.text_end_cidade);
		EditText campoEndCep = (EditText) findViewById(R.id.text_end_cep);
		
		boolean valido = true;
		
		/*Campos obrigat�rios*/
		if (GamewareUtils.campoVazio(campoNome)){
			Toast.makeText(TelaDadosCadastraisClienteActivity.this, "Voc� deve preencher o campo Nome Completo", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if ( (!(checkBoxSexoM.isChecked())) && (!(checkBoxSexoF.isChecked())) ){
			Toast.makeText(TelaDadosCadastraisClienteActivity.this, "Voc� deve preencher o campo Sexo", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(campoEmail)){
			Toast.makeText(TelaDadosCadastraisClienteActivity.this, "Voc� deve preencher o campo E-Mail", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(campoTelefoneRes) && GamewareUtils.campoVazio(campoTelefoneCel)){
			Toast.makeText(TelaDadosCadastraisClienteActivity.this, "Voc� deve preencher o campo Telefone Residencial ou Telefone Celular", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(campoEndRua)){
			Toast.makeText(TelaDadosCadastraisClienteActivity.this, "Voc� deve preencher o campo Rua", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(campoEndNum)){
			Toast.makeText(TelaDadosCadastraisClienteActivity.this, "Voc� deve preencher o campo N�", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(campoEndBairro)){
			Toast.makeText(TelaDadosCadastraisClienteActivity.this, "Voc� deve preencher o campo Bairro", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(campoEndCidade)){
			Toast.makeText(TelaDadosCadastraisClienteActivity.this, "Voc� deve preencher o campo Cidade", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(campoEndCep)){
			Toast.makeText(TelaDadosCadastraisClienteActivity.this, "Voc� deve preencher o campo Cep", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(campoDataNascimento)){
			Toast.makeText(TelaDadosCadastraisClienteActivity.this, "Voc� deve preencher o campo Data de Nascimento", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		
		/*Valida��o data de nascimento*/
		if (!("".equals(campoDataNascimento.getText().toString()))){
			if (!GamewareUtils.validaData(campoDataNascimento.getText().toString())){
				Toast.makeText(TelaDadosCadastraisClienteActivity.this, "A Data de Nascimento deve estar no formato DD/MM/AAAA. Digite novamente.", Toast.LENGTH_SHORT).show();
				valido=false;
			}
		}
		
		return valido;
	}
	
	public void alertaMessageOk(String mensagem){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// set dialog message
		alertDialogBuilder
			.setMessage(mensagem)
			.setCancelable(false)
			.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					finish();
					Intent in = new Intent(getApplicationContext(), TelaMenuGerenteActivity.class);
					startActivity(in);
				}
			  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
	}
	
	public void alertaMessageConfirmAlteracao (String mensagemQuestion, final String mensagemSucess){

		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(mensagemQuestion)
				.setCancelable(false)
				.setPositiveButton(Constants.BUTTON_SIM,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								String mensagemSucessAux = mensagemSucess;
								//TODO: Opera��o de insert
								EditText textFieldNomeCompleto = (EditText) findViewById(R.id.text_nome_completo);
								CheckBox checkBoxSexoF = (CheckBox) findViewById(R.id.checkBox_sexo_F);
								EditText textFieldDataNascimento = (EditText) findViewById(R.id.text_data_nascimento);
								EditText textFieldRg = (EditText) findViewById(R.id.text_rg_cliente);
								EditText textFieldEmail = (EditText) findViewById(R.id.text_email);
								EditText textFieldTelefoneRes = (EditText) findViewById(R.id.text_telefone_res);
								EditText textFieldTelefoneCel = (EditText) findViewById(R.id.text_telefone_cel);
								EditText textFieldCpf = (EditText) findViewById(R.id.text_cpf);
								EditText textFieldSkype = (EditText) findViewById(R.id.text_skype);
								EditText campoEndRua = (EditText) findViewById(R.id.text_end_rua);
								EditText campoEndNum = (EditText) findViewById(R.id.text_end_num);
								EditText campoEndBairro = (EditText) findViewById(R.id.text_end_bairro);
								EditText campoEndCidade = (EditText) findViewById(R.id.text_end_cidade);
								EditText campoEndCep = (EditText) findViewById(R.id.text_end_cep);
								EditText campoFacebook = (EditText) findViewById(R.id.text_facebook);
								EditText campoGtalk = (EditText) findViewById(R.id.text_gtalk);
								EditText campoWhatsApp = (EditText) findViewById(R.id.text_whatsapp);
								EditText campoEndCompl = (EditText) findViewById(R.id.text_end_compl);
								Spinner campoEstado = (Spinner) findViewById(R.id.seletor_estado);
								
								SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
								
								CadastroCliente cadastroCliente = new CadastroCliente();
								cadastroCliente.setNome(GamewareUtils.stringEditText(textFieldNomeCompleto));
								if (checkBoxSexoF.isChecked()){
									cadastroCliente.setSexo(Constants.CLIENTE_SEXO_F);
								}else{
									cadastroCliente.setSexo(Constants.CLIENTE_SEXO_M);
								}
								try {
									cadastroCliente.setDataNascimento(sdf.parse(GamewareUtils.stringEditText(textFieldDataNascimento)));
								} catch (ParseException e) {
									e.printStackTrace();
								}
								
								cadastroCliente.setRg(Integer.valueOf(MaskUtils.unmask(GamewareUtils.stringEditText(textFieldRg))));
								cadastroCliente.setEmail(GamewareUtils.stringEditText(textFieldEmail));
								cadastroCliente.setTelefone(GamewareUtils.stringEditText(textFieldTelefoneRes));
								cadastroCliente.setTelefone_cel(GamewareUtils.stringEditText(textFieldTelefoneCel));
								cadastroCliente.setCpf(MaskUtils.unmask(GamewareUtils.stringEditText(textFieldCpf)));
								cadastroCliente.setSkype(GamewareUtils.stringEditText(textFieldSkype));
								cadastroCliente.setEndRua(GamewareUtils.stringEditText(campoEndRua));
								cadastroCliente.setEndNum(GamewareUtils.stringEditText(campoEndNum));
								cadastroCliente.setEndBairro(GamewareUtils.stringEditText(campoEndBairro));
								cadastroCliente.setEndCidade(GamewareUtils.stringEditText(campoEndCidade));
								cadastroCliente.setEndCep(GamewareUtils.stringEditText(campoEndCep));
								cadastroCliente.setFacebook(GamewareUtils.stringEditText(campoFacebook));
								cadastroCliente.setGtalk(GamewareUtils.stringEditText(campoGtalk));
								cadastroCliente.setWhatsapp(GamewareUtils.stringEditText(campoWhatsApp));
								cadastroCliente.setEndCompl(GamewareUtils.stringEditText(campoEndCompl));
								cadastroCliente.setEndEstado(campoEstado.getSelectedItem().toString());
								
								Intent it = getIntent();
								Bundle bundle = it.getExtras();
								Integer idCliente = bundle.getInt(Constants.SESSION_INDICE_SELECIONADO);
								
								Status status = new Status();
								try {
									status = CelulaREST.alteraCliente(String.valueOf(idCliente), cadastroCliente.getNome(), cadastroCliente.getSexo(), sdf.format(cadastroCliente.getDataNascimento()), cadastroCliente.getRg(), cadastroCliente.getCpf(), cadastroCliente.getEndRua(), cadastroCliente.getEndNum(), cadastroCliente.getEndCompl(), cadastroCliente.getEndBairro(), cadastroCliente.getEndCep(), cadastroCliente.getEndCidade(), cadastroCliente.getEndEstado(), cadastroCliente.getEmail(), cadastroCliente.getTelefone(), cadastroCliente.getTelefone_cel(), cadastroCliente.getSkype(), cadastroCliente.getFacebook(), cadastroCliente.getGtalk(), cadastroCliente.getWhatsapp());
								} catch (Exception e) {
									status.setCodigoErro(Constants.ERRO_OPERACAO_BD);
								}
								if (Constants.SUCESSO_OPERACAO_BD.equals(status.getCodigoErro())){
									
								}else if (Constants.ERRO_OPERACAO_BD.equals(status.getCodigoErro())){
									alertaMessageErro(Constants.MESSAGE_ERRO_BD);
								}
							
							final String mensagemAux2 = mensagemSucessAux;
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TelaDadosCadastraisClienteActivity.this);
									// set dialog message
									alertDialogBuilder
										.setMessage(mensagemAux2)
										.setCancelable(false)
										.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) {
												if (Constants.MESSAGE_ERRO_BD.equals(mensagemAux2)){
													dialog.cancel();
												}else{
													finish();
												}
											}
										  });
						 
										// create alert dialog
										AlertDialog alertDialog = alertDialogBuilder.create();
						 
										// show it
										alertDialog.show();
							}
						})
				.setNegativeButton(Constants.BUTTON_NAO,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	
	}
	
	public void alertaMessageConfirmExclusao (String mensagemQuestion, final String mensagemSucess){

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(mensagemQuestion)
				.setCancelable(false)
				.setPositiveButton(Constants.BUTTON_SIM,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								String mensagemSucessAux = mensagemSucess;
								
								//TODO: Realizar a exclusao com o indice selecionado
								Intent it = getIntent();
								Bundle bundle = it.getExtras();
								Integer indiceSelecionado = bundle.getInt(Constants.SESSION_INDICE_SELECIONADO);
								Status status = new Status();
								try {
									status = CelulaREST.excluiCliente(indiceSelecionado);
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								if (Constants.SUCESSO_OPERACAO_BD.equals(status.getCodigoErro())){
									
								}else if (Constants.ERRO_OPERACAO_BD.equals(status.getCodigoErro())){
									mensagemSucessAux = Constants.MESSAGE_ERRO_BD;
								}
								
								final String mensagemAux2 = mensagemSucessAux;
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TelaDadosCadastraisClienteActivity.this);
									// set dialog message
									alertDialogBuilder
										.setMessage(mensagemAux2)
										.setCancelable(false)
										.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) {
												if (Constants.MESSAGE_ERRO_BD.equals(mensagemAux2)){
													dialog.cancel();
												}else{
													finish();
												}
											}
										  });
						 
										// create alert dialog
										AlertDialog alertDialog = alertDialogBuilder.create();
						 
										// show it
										alertDialog.show();
							}
						})
				.setNegativeButton(Constants.BUTTON_NAO,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	
	}
	
	public void alertaMessageErro(String mensagem){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// set dialog message
		alertDialogBuilder
			.setMessage(mensagem)
			.setCancelable(false)
			.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.cancel();
				}
			  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		finish();
	}

}
