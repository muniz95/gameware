package com.gameware.tcc2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gameware.tcc2.model.Console;
import com.gameware.tcc2.model.FichaProduto;
import com.gameware.tcc2.model.Status;
import com.gameware.tcc2.model.TipoEquipamento;
import com.gameware.tcc2.utils.CelulaREST;
import com.gameware.tcc2.utils.Constants;
import com.gameware.tcc2.utils.GamewareUtils;

public class TelaCadastroProduto extends Activity {

	@SuppressWarnings("rawtypes")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_cadastro_produto);
		Spinner seletorConsoleAssociado = (Spinner) findViewById(R.id.seletor_console_associado);
		TextView label_seletor_console = (TextView) findViewById(R.id.label_console_associado);
		Spinner seletorCategoria = (Spinner)findViewById(R.id.seletor_categoria);
		boolean isConsole=false;
		
		List<String> categoriaList = new ArrayList<String>();
		List<TipoEquipamento> categoriaWSList  = new ArrayList<TipoEquipamento>();
		
		
		try {
			categoriaWSList = CelulaREST.listaCategorias();
			for (Iterator iterator = categoriaWSList.iterator(); iterator.hasNext();) {
				TipoEquipamento tipoEquipamento = (TipoEquipamento) iterator.next();
				categoriaList.add(tipoEquipamento.getNomeTipo());
				
			}
		} catch (Exception e) {
			alertaMessageErro(Constants.MESSAGE_ERRO_BD);
		}
		
		final List<TipoEquipamento> listaCategoriaResponse = categoriaWSList;
		Spinner seletor_categoria = (Spinner)findViewById(R.id.seletor_categoria);
		seletor_categoria.setOnItemSelectedListener(new OnItemSelectedListener() {

			Intent it = getIntent();
			Bundle bundle = new Bundle();
			
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				bundle.putInt(Constants.SESSION_INDICE_SELECIONADO_CATEGORIA, listaCategoriaResponse.get(arg2).getIdTipo());
				it.putExtras(bundle);
				
				TextView label_qtde_estoque = (TextView)findViewById(R.id.label_qtde_estoque);
				EditText text_qtde_estoque = (EditText)findViewById(R.id.text_qtde_estoque);
				TextView label_valor = (TextView)findViewById(R.id.label_valor);
				EditText text_valor = (EditText)findViewById(R.id.text_valor);
				TextView label_seletor_console = (TextView) findViewById(R.id.label_console_associado);
				Spinner seletorConsoleAssociado = (Spinner) findViewById(R.id.seletor_console_associado);
				CheckBox checkbox_associado = (CheckBox) findViewById(R.id.checkbox_nao_associado);
				
				if ("Console".equals(listaCategoriaResponse.get(arg2).getNomeTipo())){
					label_valor.setVisibility(View.GONE);
					text_valor.setVisibility(View.GONE);
					label_qtde_estoque.setVisibility(View.GONE);
					text_qtde_estoque.setVisibility(View.GONE);
					checkbox_associado.setVisibility(View.GONE);
					seletorConsoleAssociado.setEnabled(true);
				}else{
					label_valor.setVisibility(View.VISIBLE);
					text_valor.setVisibility(View.VISIBLE);
					label_qtde_estoque.setVisibility(View.VISIBLE);
					text_qtde_estoque.setVisibility(View.VISIBLE);
					checkbox_associado.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
		});
		
		List<String> consolesList = new ArrayList<String>();
		List<Console> consolesWSList  = new ArrayList<Console>();
		
		
		try {
			consolesWSList = CelulaREST.listaConsoles();
			for (Iterator iterator = consolesWSList.iterator(); iterator.hasNext();) {
				Console console = (Console) iterator.next();
				consolesList.add(console.getNomeConsole());
			}
		} catch (Exception e) {
			alertaMessageErro(Constants.MESSAGE_ERRO_BD);
		}

		final List<Console> listaConsolesResponse = consolesWSList;
		Spinner seletor_console = (Spinner)findViewById(R.id.seletor_console_associado);
		seletor_console.setOnItemSelectedListener(new OnItemSelectedListener() {

			Intent it = getIntent();
			Bundle bundle = new Bundle();
			
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				bundle.putInt(Constants.SESSION_INDICE_SELECIONADO_CONSOLE, listaConsolesResponse.get(arg2).getIdConsole());
				it.putExtras(bundle);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
		});	
		
		ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, categoriaList);
        seletor_categoria.setAdapter(adapter);
        
		ArrayAdapter adapter1 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, consolesList);
        seletor_console.setAdapter(adapter1);
	}

	public void onButtonClick (View v){

		Boolean isConsole = false;
		Boolean valido = true;
		EditText textNomeProduto = (EditText) findViewById(R.id.text_nome_produto);
		EditText textQuantidadeEstoque = (EditText) findViewById(R.id.text_qtde_estoque);
		EditText textValor = (EditText) findViewById(R.id.text_valor);
		Spinner seletorCategoria = (Spinner) findViewById(R.id.seletor_categoria);
		Spinner seletorConsoleAssociado = (Spinner) findViewById(R.id.seletor_console_associado);
		CheckBox checkbox_nao_associado = (CheckBox) findViewById(R.id.checkbox_nao_associado);
		TextView label_seletor_console = (TextView) findViewById(R.id.label_console_associado);
		
		if("Console".equals(seletorCategoria.getSelectedItem().toString())){
			isConsole = true;
		}
		
		
		if (R.id.checkbox_nao_associado==v.getId()){
			if (checkbox_nao_associado.isChecked()){
				seletorConsoleAssociado.setEnabled(false);
			}else{
				seletorConsoleAssociado.setEnabled(true);
			}
		}
		
		if("Console".equals(seletorCategoria.getSelectedItem().toString())){
			isConsole = true;
		}
		
		if (R.id.button_ok==v.getId()){
			if (GamewareUtils.campoVazio(textNomeProduto)){
				Toast.makeText(TelaCadastroProduto.this, "Voc� deve preencher o campo Nome do Produto", Toast.LENGTH_SHORT).show();
				valido = false;
			}
			
			if (!isConsole){
				if (GamewareUtils.campoVazio(textQuantidadeEstoque)){
					Toast.makeText(TelaCadastroProduto.this, "Voc� deve preencher o campo Quantidade no Estoque", Toast.LENGTH_SHORT).show();
					valido = false;
				}
				if (GamewareUtils.campoVazio(textValor)){
					Toast.makeText(TelaCadastroProduto.this, "Voc� deve preencher o campo Valor", Toast.LENGTH_SHORT).show();
					valido = false;
				}
			}
			if (valido){
				boolean sucesso = true;
				//TODO: Opera��o de insert
				FichaProduto fichaProduto = new FichaProduto();
				
				textNomeProduto = (EditText) findViewById(R.id.text_nome_produto);
				textQuantidadeEstoque = (EditText) findViewById(R.id.text_qtde_estoque);
				textValor = (EditText) findViewById(R.id.text_valor);
				
				//TODO: Chamar servi�o aqui para recuperar as categorias
				List<TipoEquipamento> tipoEquipamentoList = new ArrayList<TipoEquipamento>();
				
				fichaProduto.setNomeProduto(GamewareUtils.stringEditText(textNomeProduto));
				
				if (!isConsole){
				fichaProduto.setQuantidadeEstoque(Integer.valueOf(GamewareUtils.stringEditText(textQuantidadeEstoque)));
				fichaProduto.setValorMonetario(Float.valueOf(GamewareUtils.stringEditText(textValor)));
				}
				Intent it = getIntent();
				Bundle bundle = it.getExtras();
				
				Status status = new Status();
				
				boolean notAssociado = false;
				
				if (checkbox_nao_associado.isChecked()){
					notAssociado = true;
				}
				
				String idConsole = String.valueOf(bundle.getInt(Constants.SESSION_INDICE_SELECIONADO_CONSOLE));
				
				try {
					if (isConsole){
						status = CelulaREST.cadastraProduto(fichaProduto.getNomeProduto(), bundle.getInt(Constants.SESSION_INDICE_SELECIONADO_CATEGORIA), idConsole, Integer.valueOf(1), Float.valueOf(0), isConsole, notAssociado);
					}else{
						status = CelulaREST.cadastraProduto(fichaProduto.getNomeProduto(), bundle.getInt(Constants.SESSION_INDICE_SELECIONADO_CATEGORIA), idConsole, fichaProduto.getQuantidadeEstoque(), fichaProduto.getValorMonetario(), isConsole, notAssociado);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (Constants.SUCESSO_OPERACAO_BD.equals(status.getCodigoErro())){
					alertaMessageOk(Constants.MESSAGE_SUCESSO_CADASTRO_PRODUTO);
				}else if (Constants.ERRO_OPERACAO_BD.equals(status.getCodigoErro())){
					alertaMessageErro(Constants.MESSAGE_ERRO_BD);
				}else if (Constants.ERRO_PRODUTO_EXISTENTE.equals(status.getCodigoErro())){
					alertaMessageErro(Constants.MESSAGE_ERRO_CADASTRO_PRODUTO);
				}
			}
		}
	
	}
	public void alertaMessageOk(String mensagem){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// set dialog message
		alertDialogBuilder
			.setMessage(mensagem)
			.setCancelable(false)
			.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					finish();
				}
			  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
	}
	
	public void alertaMessageErro(String mensagem){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// set dialog message
		alertDialogBuilder
			.setMessage(mensagem)
			.setCancelable(false)
			.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.cancel();
				}
			  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		super.finish();
	}	
}