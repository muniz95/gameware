package com.gameware.tcc2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.gameware.tcc2.model.Usuario;
import com.gameware.tcc2.utils.CelulaREST;
import com.gameware.tcc2.utils.Constants;
import com.gameware.tcc2.utils.GamewareUtils;

public class TelaLogin extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	    StrictMode.setThreadPolicy(policy);
		//Remove title bar
	}

	@Override
	protected void onResume() {
		EditText login = (EditText) findViewById(R.id.text_login);
		EditText senha = (EditText) findViewById(R.id.text_senha);
		login.setText(Constants.EMPTY);
		senha.setText(Constants.EMPTY);
		super.onResume();
	}
	
	public void onButtonClick (View v){
		
		boolean valido = true;
		boolean autorizado = true;
		String tipoUsuario = "";
		EditText login = (EditText) findViewById(R.id.text_login);
		EditText senha = (EditText) findViewById(R.id.text_senha);
		
		/*Verifica se os campos est�o preenchidos*/
		if (R.id.button_conectar==v.getId()){
			
			if (login.getText().toString().equals(Constants.EMPTY)){
				Toast.makeText(TelaLogin.this, "Informe o Login", Toast.LENGTH_SHORT).show();
				valido = false;
			}
			else if (senha.getText().toString().equals(Constants.EMPTY)){
				Toast.makeText(TelaLogin.this, "Informe a Senha", Toast.LENGTH_SHORT).show();
				valido = false;
			}
			
			if (valido) {
				//TODO: Retornar login do WS
				
				try {
					CelulaREST ws = new CelulaREST();
					Usuario usuario = ws.login(GamewareUtils.stringEditText(login), GamewareUtils.stringEditText(senha));
					
					if (Constants.WS_SEM_REGISTROS==usuario.getIdUsuario()){
						autorizado=false;
					}else{
						tipoUsuario=usuario.getTipoUsuario();
					}

				} catch (Exception e) {
					e.printStackTrace();
					Toast.makeText(TelaLogin.this, "Erro na rede. Tente novamente.", Toast.LENGTH_SHORT).show();
				}
			}
		
			if (autorizado){
				if (Constants.TIPO_ACESSO_GERENTE.equals(tipoUsuario)){
					Intent intent = new Intent(TelaLogin.this,TelaMenuGerenteActivity.class);
					startActivity(intent);
				}
				else if (Constants.TIPO_ACESSO_ADMINISTRADOR.equals(tipoUsuario)){
					Intent intent = new Intent(TelaLogin.this,TelaMenuAdministradorActivity.class);
					startActivity(intent);
				}
			}else{
				Toast.makeText(TelaLogin.this, "Login e/ou senha incorretos.", Toast.LENGTH_SHORT).show();
			}
		}
	}
}