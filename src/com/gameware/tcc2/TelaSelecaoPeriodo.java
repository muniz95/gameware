package com.gameware.tcc2;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gameware.tcc2.utils.Constants;
import com.gameware.tcc2.utils.GamewareUtils;
import com.gameware.tcc2.utils.MaskUtils;

public class TelaSelecaoPeriodo extends FragmentActivity implements
		ActionBar.TabListener {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_selecao_periodo);
		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}
		
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tela_selecao_periodo, menu);
		return true;
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Fragment fragment = new DummySectionFragment();
			Bundle args = new Bundle();
			args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position);
			fragment.setArguments(args);
			
			return fragment;
		}

		@Override
		public int getCount() {
			// Show 4 total pages.
			return 4;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			case 3:
				return getString(R.string.title_section4).toUpperCase(l);
			}
			return null;
		}
	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class DummySectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";
		private TextWatcher dtIniMask;
		private TextWatcher dtFinMask;
		
		public DummySectionFragment() {
		}

		@Override
		public void onStart(){
			super.onStart();
			inicia();
		}
		
		public void onButtonClick(View v) throws ParseException{}
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_tela_selecao_periodo_dummy, container,
					false);
			
			
			return rootView;
		}
		public void inicia(){
			final Integer periodOption = getArguments().getInt(
					ARG_SECTION_NUMBER);
			TextView textView = (TextView) getView().findViewById(R.id.textView_selecione_periodo);
			DatePicker datePicker = (DatePicker) getView().findViewById(R.id.seletor_data);
			Button buttonOk = (Button) getView().findViewById(R.id.button_ok);
			TextView textView_dataInicial = (TextView) getView().findViewById(R.id.textView_dataInicial);
			TextView textView_dataFinal = (TextView) getView().findViewById(R.id.textView_dataFinal);
			EditText text_dataInicial = (EditText) getView().findViewById(R.id.text_dataInicial);
			EditText text_dataFinal = (EditText) getView().findViewById(R.id.text_dataFinal);
			Button buttonOk1 = (Button) getView().findViewById(R.id.button_ok1);
			
			dtIniMask = MaskUtils.insert(Constants.MASK_FORMAT_DATA, text_dataInicial);
			text_dataInicial.addTextChangedListener(dtIniMask);
			dtFinMask = MaskUtils.insert(Constants.MASK_FORMAT_DATA, text_dataFinal);
			text_dataFinal.addTextChangedListener(dtFinMask);
			
			buttonOk1.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					boolean errorFound=false;
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					EditText dataInicial = (EditText) getView().findViewById(R.id.text_dataInicial);
					EditText dataFinal = (EditText) getView().findViewById(R.id.text_dataFinal);
					
					String dataConsulta = "";
					String dataConsulta1 = "";
					dataConsulta = dataInicial.getText().toString();
					dataConsulta1 = dataFinal.getText().toString();
					
					if (dataConsulta.isEmpty()){
						Toast.makeText(getView().getContext().getApplicationContext(), "Voc� deve preencher o campo 'Data inicial'", Toast.LENGTH_SHORT).show();
						errorFound=true;
					}
					if (dataConsulta1.isEmpty()){
						Toast.makeText(getView().getContext().getApplicationContext(), "Voc� deve preencher o campo 'Data final'", Toast.LENGTH_SHORT).show();
						errorFound=true;
					}
						if (!errorFound) {
							if (!GamewareUtils.validaData(dataConsulta)) {
								Toast.makeText(getView().getContext().getApplicationContext(), "Data inicial inv�lida", Toast.LENGTH_SHORT).show();
								errorFound = true;
							}
							if (!GamewareUtils.validaData(dataConsulta1)) {
								Toast.makeText(getView().getContext().getApplicationContext(), "Data final inv�lida", Toast.LENGTH_SHORT).show();
								errorFound = true;
							}
							try {
								if (GamewareUtils.dataMaiorAtual(
										dataConsulta.substring(0, 2),
										dataConsulta.substring(3, 5),
										dataConsulta.substring(6, 10))) {
									Toast.makeText(getView().getContext().getApplicationContext(), "Informe uma data inicial menor ou igual � data atual", Toast.LENGTH_SHORT).show();
									errorFound = true;
								}
							} catch (ParseException e) {
								e.printStackTrace();
							}
							try {
								if (GamewareUtils.dataMaiorAtual(
										dataConsulta1.substring(0, 2),
										dataConsulta1.substring(3, 5),
										dataConsulta1.substring(6, 10))) {
									Toast.makeText(getView().getContext().getApplicationContext(), "Informe uma data final menor ou igual � data atual", Toast.LENGTH_SHORT).show();
									errorFound = true;
								}
							} catch (ParseException e) {
								e.printStackTrace();
							}

							Date data = null;
							try {
								data = sdf.parse(dataConsulta);
							} catch (ParseException e) {
								e.printStackTrace();
							}
							Date data1 = null;
							try {
								data1 = sdf.parse(dataConsulta1);
							} catch (ParseException e) {
								e.printStackTrace();
							}

							if (data.after(data1)) {
								Toast.makeText(
										getView().getContext()
												.getApplicationContext(),
										"Per�odo Inv�lido.",
										Toast.LENGTH_SHORT).show();
								errorFound = true;
							}
						}
						
						if (!errorFound){
							
							//TODO: Implementa��o webservice aqui - soma do faturamento
							Double somaFaturamento = 0.00;
							
							Bundle bundle = new Bundle();
							bundle.putString(Constants.SESSION_DATE_QUERY, dataConsulta);
							bundle.putString(Constants.SESSION_DATE_QUERY1, dataConsulta1);
							bundle.putInt(Constants.SESSION_PERIOD_OPTION, periodOption);
							bundle.putDouble(Constants.SESSION_RENDIMENTO_TOTAL_PERIODO, somaFaturamento);
							Intent intent = new Intent(getView().getContext().getApplicationContext(),TelaFaturamentoPeriodoActivity.class);
							intent.putExtras(bundle);
							startActivity(intent);		
						}

				}
		  
			
				
			});
			
			buttonOk.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {

					Integer periodOption = getArguments().getInt(
							ARG_SECTION_NUMBER);

					DatePicker datePicker = (DatePicker)getView().findViewById(R.id.seletor_data);
					EditText dataInicial = (EditText) getView().findViewById(R.id.text_dataInicial);
					EditText dataFinal = (EditText) getView().findViewById(R.id.text_dataFinal);
					
					String dataConsulta = "";
					String dataConsulta1 = "";
					boolean errorFound = false;
				if (R.id.button_ok==v.getId()){	
					if (periodOption==Constants.PERIODO_DIA){
						Integer dia = datePicker.getDayOfMonth();
						Integer mes = datePicker.getMonth()+1;
						Integer ano = datePicker.getYear();
						
						try {
							if (GamewareUtils.dataMaiorAtual(GamewareUtils.adicionaZeroEsquerdaDiaMes(dia),GamewareUtils.adicionaZeroEsquerdaDiaMes(mes), String.valueOf(ano))){
								Toast.makeText(getView().getContext().getApplicationContext(), "Data Inv�lida.", Toast.LENGTH_SHORT).show();
								errorFound=true;
							}
						} catch (ParseException e) {
							e.printStackTrace();
						}
						
						dataConsulta = GamewareUtils.adicionaZeroEsquerdaDiaMes(dia)+GamewareUtils.adicionaZeroEsquerdaDiaMes(mes)+String.valueOf(ano);
						dataConsulta1 = GamewareUtils.adicionaZeroEsquerdaDiaMes(dia)+GamewareUtils.adicionaZeroEsquerdaDiaMes(mes)+String.valueOf(ano);
						
						SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
						SimpleDateFormat sdfDesiredFormat = new SimpleDateFormat("dd/MM/yyyy");
						try {
							Date dataAux = sdf.parse(dataConsulta);
							dataConsulta = sdfDesiredFormat.format(dataAux);
							dataAux = sdf.parse(dataConsulta1);
							dataConsulta1 = sdfDesiredFormat.format(dataAux);
						} catch (ParseException e) {
							e.printStackTrace();
						}
						
					}else if (periodOption==Constants.PERIODO_MES){
						Integer mes = datePicker.getMonth()+1;
						Integer ano = datePicker.getYear();
						
						try {
							if (GamewareUtils.dataMaiorAtual(GamewareUtils.adicionaZeroEsquerdaDiaMes(mes), String.valueOf(ano))){
								Toast.makeText(getView().getContext().getApplicationContext(), "Data Inv�lida.", Toast.LENGTH_SHORT).show();
								errorFound=true;
							}
						} catch (ParseException e) {
							e.printStackTrace();
						}
						
						dataConsulta = "01"+GamewareUtils.adicionaZeroEsquerdaDiaMes(mes)+String.valueOf(ano);
						dataConsulta1 = "30"+GamewareUtils.adicionaZeroEsquerdaDiaMes(mes)+String.valueOf(ano);
						
						SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
						SimpleDateFormat sdfDesiredFormat = new SimpleDateFormat("dd/MM/yyyy");
						Date dataAux = new Date();
						try {
							dataAux = sdf.parse(dataConsulta);
							dataConsulta = sdfDesiredFormat.format(dataAux);
						} catch (ParseException e) {
							e.printStackTrace();
						}
						
						Calendar cal = GregorianCalendar.getInstance();
						cal.setTime(dataAux);
						         
						int diaCalculo = cal.getActualMaximum( Calendar.DAY_OF_MONTH );
						int mesCalculo = (cal.get(Calendar.MONDAY)+1);
						int anoCalculo = cal.get(Calendar.YEAR);
						         
						try {
						    Date data = (new SimpleDateFormat("dd/MM/yyyy")).parse( diaCalculo+"/"+mesCalculo+"/"+anoCalculo );
						    dataConsulta1 = sdfDesiredFormat.format(data);
						} catch (ParseException e) {
						    e.printStackTrace();
						}
						
						
					}else if (periodOption==Constants.PERIODO_ANO){
						Integer ano = datePicker.getYear();
						
						dataConsulta = "01/01/"+ano;
						dataConsulta1 = "31/12/"+ano;

						try {
							if (GamewareUtils.dataMaiorAtual(String.valueOf(ano))){
								Toast.makeText(getView().getContext().getApplicationContext(), "Ano Inv�lido.", Toast.LENGTH_SHORT).show();
								errorFound=true;
							}
						} catch (ParseException e) {
							e.printStackTrace();
						}
						
					}else if (periodOption==Constants.PERIODO_PER){}
					if (!errorFound){
					Bundle bundle = new Bundle();
					bundle.putString(Constants.SESSION_DATE_QUERY, dataConsulta);
					bundle.putString(Constants.SESSION_DATE_QUERY1, dataConsulta1);
					bundle.putInt(Constants.SESSION_PERIOD_OPTION, periodOption);
					Intent intent = new Intent(getView().getContext().getApplicationContext(),TelaFaturamentoPeriodoActivity.class);
					intent.putExtras(bundle);
					startActivity(intent);					
					}
				}
			  
				
					
				}
			});
			
			if (periodOption==Constants.PERIODO_DIA){
				datePicker.setVisibility(View.VISIBLE);
				textView.setText("Selecione o dia desejado");
			}else if (periodOption==Constants.PERIODO_MES){
				datePicker.setVisibility(View.VISIBLE);
				try {
					Field f[] = datePicker.getClass().getDeclaredFields();
					for (Field field : f) {
						if (field.getName().equals("mDaySpinner")) {
							field.setAccessible(true);
							Object dayPicker = new Object();
							dayPicker = field.get(datePicker);
							((View) dayPicker).setVisibility(View.GONE);
						}
					}
				} catch (SecurityException e) {
					Log.d("ERROR", e.getMessage());
				} 
				catch (IllegalArgumentException e) {
					Log.d("ERROR", e.getMessage());
				} catch (IllegalAccessException e) {
					Log.d("ERROR", e.getMessage());
				}
				textView.setText("Selecione o m�s desejado");
			}else if (periodOption==Constants.PERIODO_ANO){
				datePicker.setVisibility(View.VISIBLE);
				try {
					Field f[] = datePicker.getClass().getDeclaredFields();
					for (Field field : f) {
						if (field.getName().equals("mDaySpinner")) {
							field.setAccessible(true);
							Object dayPicker = new Object();
							dayPicker = field.get(datePicker);
							((View) dayPicker).setVisibility(View.GONE);
						}
						if (field.getName().equals("mMonthSpinner")) {
							field.setAccessible(true);
							Object dayPicker = new Object();
							dayPicker = field.get(datePicker);
							((View) dayPicker).setVisibility(View.GONE);
						}
					}
				} catch (SecurityException e) {
					Log.d("ERROR", e.getMessage());
				} 
				catch (IllegalArgumentException e) {
					Log.d("ERROR", e.getMessage());
				} catch (IllegalAccessException e) {
					Log.d("ERROR", e.getMessage());
				}
				textView.setText("Selecione o ano desejado");
			}else if (periodOption==Constants.PERIODO_PER){
				datePicker.setVisibility(View.GONE);
				buttonOk.setVisibility(View.GONE);
				textView_dataInicial.setVisibility(View.VISIBLE);
				textView_dataFinal.setVisibility(View.VISIBLE);
				text_dataInicial.setVisibility(View.VISIBLE);
				text_dataFinal.setVisibility(View.VISIBLE);
				buttonOk1.setVisibility(View.VISIBLE);
			}
		}

		
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		super.finish();
	}	
}
