package com.gameware.tcc2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class TelaConfirmarSenhaAdmin extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_confirmar_senha_admin);

	}

	public void onButtonClick(View v) {

		EditText senha = (EditText) findViewById(R.id.text_nova_senha);

		//TODO: validar senha do admin
		
		if ("1".equals(senha.getText().toString())) {
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					TelaConfirmarSenhaAdmin.this);
			// set dialog message
			alertDialogBuilder
					.setMessage("Senha alterada com sucesso!")
					.setCancelable(false)
					.setNeutralButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									finish();
								}
							});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
		} else {
			Toast.makeText(TelaConfirmarSenhaAdmin.this, "Senha incorreta! Tente novamente.", Toast.LENGTH_SHORT).show();
		}

	}
	@Override
	public void onDestroy(){
		super.onDestroy();
		super.finish();
	}
}
