package com.gameware.tcc2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.gameware.tcc2.model.Desconto;
import com.gameware.tcc2.utils.CelulaREST;
import com.gameware.tcc2.utils.Constants;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

public class TelaSelecaoDescontoActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_selecao_desconto);
		

		
		List<String> listaDesconto = new ArrayList<String>();
		List<Desconto> descontosList = new ArrayList<Desconto>();
		try {
			descontosList = CelulaREST.listarTodosDescontos();
		} catch (Exception e) {
			e.printStackTrace();
		}
		final List<Desconto>listaDescontoResponse = descontosList;
		
		
		//TODO: Lista de Descontos
		for (Iterator iterator = listaDescontoResponse.iterator(); iterator.hasNext();) {
			Desconto Desconto = (Desconto) iterator.next();
			listaDesconto.add(Desconto.getNomeDesconto());
		}
		
		
		Spinner s = (Spinner) findViewById(R.id.seletor_desconto);
		s.setOnItemSelectedListener(new OnItemSelectedListener() {

			Intent it = getIntent();
			Bundle bundle = new Bundle();
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				//TODO: Verificar se o selectedItemPosition do spinner esta correto
				bundle.putInt(Constants.SESSION_INDICE_SELECIONADO, listaDescontoResponse.get(arg2).getIdDesconto());
				it.putExtras(bundle);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}

		});
		
		@SuppressWarnings("unchecked")
		ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, listaDesconto);
        s.setAdapter(adapter);
	}

	public void onButtonClick (View v){
		Spinner s = (Spinner) findViewById(R.id.seletor_desconto);
		if (R.id.button_ok==v.getId()){
			finish();
			Intent intent = getIntent();
			Bundle bundle = intent.getExtras();
			System.out.println(bundle.getInt(Constants.SESSION_INDICE_SELECIONADO));
			Intent in = new Intent(getApplicationContext(), TelaDescontoActivity.class);
			Bundle bundle2 = new Bundle();
			
			in.putExtras(bundle);
			startActivity(in);}
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		super.finish();
	}
}
