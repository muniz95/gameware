package com.gameware.tcc2.model;

import java.util.Date;

public class FichaProduto {

	protected String nomeTipo;
	protected Integer idEquipamento;
	protected String nomeProduto;
	protected boolean isAssociadoConsole;
	protected Integer quantidadeEstoque;
	protected Integer idPreco;
	protected String nomeConsole;
	protected Date inicioValidadePreco;
	protected Date fimValidadePreco;
	protected float valorMonetario;
	protected Integer idTipo;
	protected Integer idConsole;

	public String getNomeTipo() {
		return nomeTipo;
	}

	public void setNomeTipo(String nomeTipo) {
		this.nomeTipo = nomeTipo;
	}

	public Integer getIdEquipamento() {
		return idEquipamento;
	}

	public void setIdEquipamento(Integer idEquipamento) {
		this.idEquipamento = idEquipamento;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public boolean isAssociadoConsole() {
		return isAssociadoConsole;
	}

	public void setAssociadoConsole(boolean isAssociadoConsole) {
		this.isAssociadoConsole = isAssociadoConsole;
	}

	public Integer getQuantidadeEstoque() {
		return quantidadeEstoque;
	}

	public void setQuantidadeEstoque(Integer quantidadeEstoque) {
		this.quantidadeEstoque = quantidadeEstoque;
	}

	public Integer getIdPreco() {
		return idPreco;
	}

	public void setIdPreco(Integer idPreco) {
		this.idPreco = idPreco;
	}

	public Date getInicioValidadePreco() {
		return inicioValidadePreco;
	}

	public void setInicioValidadePreco(Date inicioValidadePreco) {
		this.inicioValidadePreco = inicioValidadePreco;
	}

	public Date getFimValidadePreco() {
		return fimValidadePreco;
	}

	public void setFimValidadePreco(Date fimValidadePreco) {
		this.fimValidadePreco = fimValidadePreco;
	}

	public float getValorMonetario() {
		return valorMonetario;
	}

	public void setValorMonetario(float valorMonetario) {
		this.valorMonetario = valorMonetario;
	}

	public Integer getIdTipo() {
		return idTipo;
	}

	public void setIdTipo(Integer idTipo) {
		this.idTipo = idTipo;
	}

	public Integer getIdConsole() {
		return idConsole;
	}

	public void setIdConsole(Integer idConsole) {
		this.idConsole = idConsole;
	}

	public String getNomeConsole() {
		return nomeConsole;
	}

	public void setNomeConsole(String nomeConsole) {
		this.nomeConsole = nomeConsole;
	}

}
