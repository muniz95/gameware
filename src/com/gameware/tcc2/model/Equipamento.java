package com.gameware.tcc2.model;

public class Equipamento {

	protected Integer idEquipamento;
	protected String nome;
	protected boolean isAssociadoConsole;
	protected boolean emUso;
	protected Integer quantidadeEstoque;

	public Integer getIdEquipamento() {
		return idEquipamento;
	}

	public void setIdEquipamento(Integer idEquipamento) {
		this.idEquipamento = idEquipamento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isAssociadoConsole() {
		return isAssociadoConsole;
	}

	public void setAssociadoConsole(boolean isAssociadoConsole) {
		this.isAssociadoConsole = isAssociadoConsole;
	}

	public boolean isEmUso() {
		return emUso;
	}

	public void setEmUso(boolean emUso) {
		this.emUso = emUso;
	}

	public Integer getQuantidadeEstoque() {
		return quantidadeEstoque;
	}

	public void setQuantidadeEstoque(Integer quantidadeEstoque) {
		this.quantidadeEstoque = quantidadeEstoque;
	}


}