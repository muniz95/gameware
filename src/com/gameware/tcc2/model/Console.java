package com.gameware.tcc2.model;

public class Console {

	protected Integer idConsole;
	protected String nomeConsole;
	protected float valorMonetario;

	public Integer getIdConsole() {
		return idConsole;
	}

	public void setIdConsole(Integer idConsole) {
		this.idConsole = idConsole;
	}

	public String getNomeConsole() {
		return nomeConsole;
	}

	public void setNomeConsole(String nomeConsole) {
		this.nomeConsole = nomeConsole;
	}

	public float getValorMonetario() {
		return valorMonetario;
	}

	public void setValorMonetario(float valorMonetario) {
		this.valorMonetario = valorMonetario;
	}

}