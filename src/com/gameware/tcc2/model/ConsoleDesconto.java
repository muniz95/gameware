package com.gameware.tcc2.model;

import java.util.Date;

public class ConsoleDesconto {
	
	protected Integer idConsole;
	protected String nomeConsole;
	protected float valorMonetario;
	protected int idDesconto;
	protected int porcentagemDesconto;
	protected Date inicioValidade;
	protected Date fimValidade;
	protected String nomeDesconto;
	
	public Integer getIdConsole() {
		return idConsole;
	}
	public void setIdConsole(Integer idConsole) {
		this.idConsole = idConsole;
	}
	public String getNomeConsole() {
		return nomeConsole;
	}
	public void setNomeConsole(String nomeConsole) {
		this.nomeConsole = nomeConsole;
	}
	public float getValorMonetario() {
		return valorMonetario;
	}
	public void setValorMonetario(float valorMonetario) {
		this.valorMonetario = valorMonetario;
	}
	public int getIdDesconto() {
		return idDesconto;
	}
	public void setIdDesconto(int idDesconto) {
		this.idDesconto = idDesconto;
	}
	public int getPorcentagemDesconto() {
		return porcentagemDesconto;
	}
	public void setPorcentagemDesconto(int porcentagemDesconto) {
		this.porcentagemDesconto = porcentagemDesconto;
	}
	public Date getInicioValidade() {
		return inicioValidade;
	}
	public void setInicioValidade(Date inicioValidade) {
		this.inicioValidade = inicioValidade;
	}
	public Date getFimValidade() {
		return fimValidade;
	}
	public void setFimValidade(Date fimValidade) {
		this.fimValidade = fimValidade;
	}
	public String getNomeDesconto() {
		return nomeDesconto;
	}
	public void setNomeDesconto(String nomeDesconto) {
		this.nomeDesconto = nomeDesconto;
	}
	


}
