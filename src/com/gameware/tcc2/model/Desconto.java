package com.gameware.tcc2.model;

import java.util.Date;

public class Desconto {

	protected int idDesconto;
	protected int porcentagemDesconto;
	protected Date inicioValidade;
	protected Date fimValidade;
	protected String nomeDesconto;

	public int getIdDesconto() {
		return idDesconto;
	}

	public void setIdDesconto(int idDesconto) {
		this.idDesconto = idDesconto;
	}

	public int getPorcentagemDesconto() {
		return porcentagemDesconto;
	}

	public void setPorcentagemDesconto(int porcentagemDesconto) {
		this.porcentagemDesconto = porcentagemDesconto;
	}

	public Date getInicioValidade() {
		return inicioValidade;
	}

	public void setInicioValidade(Date inicioValidade) {
		this.inicioValidade = inicioValidade;
	}

	public Date getFimValidade() {
		return fimValidade;
	}

	public void setFimValidade(Date fimValidade) {
		this.fimValidade = fimValidade;
	}

	public String getNomeDesconto() {
		return nomeDesconto;
	}

	public void setNomeDesconto(String nomeDesconto) {
		this.nomeDesconto = nomeDesconto;
	}

}
