package com.gameware.tcc2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.gameware.tcc2.model.CadastroCliente;
import com.gameware.tcc2.model.Status;
import com.gameware.tcc2.utils.CelulaREST;
import com.gameware.tcc2.utils.Constants;
import com.gameware.tcc2.utils.GamewareUtils;
import com.gameware.tcc2.utils.MaskUtils;

public class TelaCadastroClienteActivity extends Activity {

	private TextWatcher cpfMask;
	private TextWatcher rgMask;
	private TextWatcher dtNascMask;
	private TextWatcher telResMask;
	private TextWatcher telCelMask;
	private TextWatcher cepMask;
	private TextWatcher whatsappMask;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_cadastro_cliente);
		Spinner campoEndEstado = (Spinner) findViewById(R.id.seletor_estado);
		
		List<String> listaEstado = new ArrayList<String>();
		listaEstado.add("AC");
		listaEstado.add("AL");
		listaEstado.add("AM");
		listaEstado.add("AP");
		listaEstado.add("BA");
		listaEstado.add("CE");
		listaEstado.add("DF");
		listaEstado.add("ES");
		listaEstado.add("GO");
		listaEstado.add("MA");
		listaEstado.add("MT");
		listaEstado.add("MS");
		listaEstado.add("MG");
		listaEstado.add("PA");
		listaEstado.add("PB");
		listaEstado.add("PE");
		listaEstado.add("PR");
		listaEstado.add("PI");
		listaEstado.add("RJ");
		listaEstado.add("RN");
		listaEstado.add("RO");
		listaEstado.add("RR");
		listaEstado.add("RS");
		listaEstado.add("SE");
		listaEstado.add("SC");
		listaEstado.add("SP");
		listaEstado.add("TO");;
		
		ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, listaEstado);
		campoEndEstado.setAdapter(adapter);
		
		EditText campoCpf = (EditText) findViewById(R.id.text_cpf);
		EditText campoRg = (EditText) findViewById(R.id.text_rg);
		EditText campoDataNascimento = (EditText) findViewById(R.id.text_data_nascimento);
		EditText campoTelRes = (EditText) findViewById(R.id.text_telefone_res);
		EditText campoTelCel = (EditText) findViewById(R.id.text_telefone_cel);
		EditText campoEndCep = (EditText) findViewById(R.id.text_end_cep);
		EditText campoWhatsApp  = (EditText) findViewById(R.id.text_whatsapp);
		cpfMask = MaskUtils.insert(Constants.MASK_FORMAT_CPF, campoCpf);
		campoCpf.addTextChangedListener(cpfMask);
		rgMask = MaskUtils.insert(Constants.MASK_FORMAT_RG, campoRg);
		campoRg.addTextChangedListener(rgMask);
		dtNascMask = MaskUtils.insert(Constants.MASK_FORMAT_DATA, campoDataNascimento);
		campoDataNascimento.addTextChangedListener(dtNascMask);
		telResMask = MaskUtils.insert(Constants.MASK_FORMAT_TELEFONE, campoTelRes);
		campoTelRes.addTextChangedListener(telResMask);
		telCelMask = MaskUtils.insert(Constants.MASK_FORMAT_TELEFONE, campoTelCel);
		campoTelCel.addTextChangedListener(telCelMask);
		cepMask = MaskUtils.insert(Constants.MASK_FORMAT_CEP, campoEndCep);
		campoEndCep.addTextChangedListener(cepMask);
		whatsappMask = MaskUtils.insert(Constants.MASK_FORMAT_TELEFONE, campoWhatsApp);
		campoWhatsApp.addTextChangedListener(whatsappMask);
	}

	public void onButtonClick (View v){
		
		CheckBox checkBoxSexoF = (CheckBox) findViewById(R.id.checkBox_sexo_F);
		CheckBox checkBoxSexoM = (CheckBox) findViewById(R.id.checkBox_sexo_M);
		
		if (R.id.checkBox_sexo_F==v.getId()){
			if (checkBoxSexoF.isChecked()){
				checkBoxSexoM.setEnabled(false);
			}
			else if (!checkBoxSexoF.isChecked()){
				checkBoxSexoM.setEnabled(true);
			}
		}else if (R.id.checkBox_sexo_M==v.getId()){
			if (checkBoxSexoM.isChecked()){
				checkBoxSexoF.setEnabled(false);
			}
			else if (!checkBoxSexoM.isChecked()){
				checkBoxSexoF.setEnabled(true);
			}
		}else if (R.id.button_ok==v.getId()){
			if (this.valido()){
				boolean sucesso = true;
				//TODO: Opera��o de insert
				
				EditText textFieldNomeCompleto = (EditText) findViewById(R.id.text_nome_completo);
				EditText textFieldDataNascimento = (EditText) findViewById(R.id.text_data_nascimento);
				EditText textFieldRg = (EditText) findViewById(R.id.text_rg);
				EditText textFieldEmail = (EditText) findViewById(R.id.text_email);
				EditText textFieldTelefoneRes = (EditText) findViewById(R.id.text_telefone_res);
				EditText textFieldTelefoneCel = (EditText) findViewById(R.id.text_telefone_cel);
				EditText textFieldCpf = (EditText) findViewById(R.id.text_cpf);
				EditText textFieldSkype = (EditText) findViewById(R.id.text_skype);
				EditText campoEndRua = (EditText) findViewById(R.id.text_end_rua);
				EditText campoEndNum = (EditText) findViewById(R.id.text_end_num);
				EditText campoEndBairro = (EditText) findViewById(R.id.text_end_bairro);
				EditText campoEndCidade = (EditText) findViewById(R.id.text_end_cidade);
				EditText campoEndCep = (EditText) findViewById(R.id.text_end_cep);
				EditText campoFacebook = (EditText) findViewById(R.id.text_facebook);
				EditText campoGtalk = (EditText) findViewById(R.id.text_gtalk);
				EditText campoWhatsApp = (EditText) findViewById(R.id.text_whatsapp);
				EditText campoEndCompl = (EditText) findViewById(R.id.text_end_compl);
				Spinner campoEstado = (Spinner) findViewById(R.id.seletor_estado);
				
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				
				CadastroCliente cadastroCliente = new CadastroCliente();
				cadastroCliente.setNome(GamewareUtils.stringEditText(textFieldNomeCompleto));
				if (checkBoxSexoF.isChecked()){
					cadastroCliente.setSexo(Constants.CLIENTE_SEXO_F);
				}else{
					cadastroCliente.setSexo(Constants.CLIENTE_SEXO_M);
				}
				try {
					cadastroCliente.setDataNascimento(sdf.parse(GamewareUtils.stringEditText(textFieldDataNascimento)));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				cadastroCliente.setRg(Integer.valueOf(MaskUtils.unmask(GamewareUtils.stringEditText(textFieldRg))));
				cadastroCliente.setEmail(GamewareUtils.stringEditText(textFieldEmail));
				cadastroCliente.setTelefone(GamewareUtils.stringEditText(textFieldTelefoneRes));
				cadastroCliente.setTelefone_cel(GamewareUtils.stringEditText(textFieldTelefoneCel));
				cadastroCliente.setCpf(MaskUtils.unmask(GamewareUtils.stringEditText(textFieldCpf)));
				cadastroCliente.setSkype(GamewareUtils.stringEditText(textFieldSkype));
				cadastroCliente.setEndRua(GamewareUtils.stringEditText(campoEndRua));
				cadastroCliente.setEndNum(GamewareUtils.stringEditText(campoEndNum));
				cadastroCliente.setEndBairro(GamewareUtils.stringEditText(campoEndBairro));
				cadastroCliente.setEndCidade(GamewareUtils.stringEditText(campoEndCidade));
				cadastroCliente.setEndCep(GamewareUtils.stringEditText(campoEndCep));
				cadastroCliente.setFacebook(GamewareUtils.stringEditText(campoFacebook));
				cadastroCliente.setGtalk(GamewareUtils.stringEditText(campoGtalk));
				cadastroCliente.setWhatsapp(GamewareUtils.stringEditText(campoWhatsApp));
				cadastroCliente.setEndCompl(GamewareUtils.stringEditText(campoEndCompl));
				cadastroCliente.setEndEstado(campoEstado.getSelectedItem().toString());
				
				Status status = new Status();
				
				try {
					status = CelulaREST.cadastraCliente(cadastroCliente.getNome(), cadastroCliente.getSexo(), sdf.format(cadastroCliente.getDataNascimento()), cadastroCliente.getRg(), cadastroCliente.getCpf(), cadastroCliente.getEndRua(), cadastroCliente.getEndNum(), cadastroCliente.getEndCompl(), cadastroCliente.getEndBairro(), cadastroCliente.getEndCep(), cadastroCliente.getEndCidade(), cadastroCliente.getEndEstado(), cadastroCliente.getEmail(), cadastroCliente.getTelefone(), cadastroCliente.getTelefone_cel(), cadastroCliente.getSkype(), cadastroCliente.getFacebook(), cadastroCliente.getGtalk(), cadastroCliente.getWhatsapp());
					if (status.getCodigoErro()==0){
						sucesso = true;
					}else{
						sucesso=false;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				if(sucesso){
					alertaMessageOk(Constants.MESSAGE_SUCESSO_CADASTRO_CLIENTE);
				}else{
					if(status.getCodigoErro()==1){
						alertaMessageErro(Constants.MESSAGE_ERRO_BD);
					}else if (status.getCodigoErro()==2){
						alertaMessageErro(Constants.MESSAGE_ERRO_USUARIO_CADASTRADO);
					}
				}
			}				
		}
	}
	
	public boolean valido(){

		CheckBox checkBoxSexoF = (CheckBox) findViewById(R.id.checkBox_sexo_F);
		CheckBox checkBoxSexoM = (CheckBox) findViewById(R.id.checkBox_sexo_M);
		EditText campoNome = (EditText) findViewById(R.id.text_nome_completo);
		EditText campoDataNascimento = (EditText) findViewById(R.id.text_data_nascimento);
		EditText campoEmail = (EditText) findViewById(R.id.text_email);
		EditText campoTelefoneRes = (EditText) findViewById(R.id.text_telefone_res);
		EditText campoTelefoneCel = (EditText) findViewById(R.id.text_telefone_cel);
		EditText campoEndRua = (EditText) findViewById(R.id.text_end_rua);
		EditText campoEndNum = (EditText) findViewById(R.id.text_end_num);
		EditText campoEndBairro = (EditText) findViewById(R.id.text_end_bairro);
		EditText campoEndCidade = (EditText) findViewById(R.id.text_end_cidade);
		EditText campoEndCep = (EditText) findViewById(R.id.text_end_cep);
		
		boolean valido = true;
		
		/*Campos obrigat�rios*/
		if (GamewareUtils.campoVazio(campoNome)){
			Toast.makeText(TelaCadastroClienteActivity.this, "Voc� deve preencher o campo Nome Completo", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if ( (!(checkBoxSexoM.isChecked())) && (!(checkBoxSexoF.isChecked())) ){
			Toast.makeText(TelaCadastroClienteActivity.this, "Voc� deve preencher o campo Sexo", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(campoEmail)){
			Toast.makeText(TelaCadastroClienteActivity.this, "Voc� deve preencher o campo E-Mail", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(campoTelefoneRes) && GamewareUtils.campoVazio(campoTelefoneCel)){
			Toast.makeText(TelaCadastroClienteActivity.this, "Voc� deve preencher o campo Telefone Residencial ou Telefone Celular", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(campoEndRua)){
			Toast.makeText(TelaCadastroClienteActivity.this, "Voc� deve preencher o campo Rua", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(campoEndNum)){
			Toast.makeText(TelaCadastroClienteActivity.this, "Voc� deve preencher o campo N�", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(campoEndBairro)){
			Toast.makeText(TelaCadastroClienteActivity.this, "Voc� deve preencher o campo Bairro", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(campoEndCidade)){
			Toast.makeText(TelaCadastroClienteActivity.this, "Voc� deve preencher o campo Cidade", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(campoEndCep)){
			Toast.makeText(TelaCadastroClienteActivity.this, "Voc� deve preencher o campo Cep", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		if (GamewareUtils.campoVazio(campoDataNascimento)){
			Toast.makeText(TelaCadastroClienteActivity.this, "Voc� deve preencher o campo Data de Nascimento", Toast.LENGTH_SHORT).show();
			valido=false;
		}
		
		/*Valida��o data de nascimento*/
		if (!("".equals(campoDataNascimento.getText().toString()))){
			if (!GamewareUtils.validaData(campoDataNascimento.getText().toString())){
				Toast.makeText(TelaCadastroClienteActivity.this, "A Data de Nascimento deve estar no formato DD/MM/AAAA. Digite novamente.", Toast.LENGTH_SHORT).show();
				valido=false;
			}
		}
		
		return valido;
	}
	
	public void alertaMessageOk(String mensagem){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// set dialog message
		alertDialogBuilder
			.setMessage(mensagem)
			.setCancelable(false)
			.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					finish();
				}
			  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
	}
	
	public void alertaMessageErro(String mensagem){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// set dialog message
		alertDialogBuilder
			.setMessage(mensagem)
			.setCancelable(false)
			.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.cancel();
				}
			  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		super.finish();
	}	
}