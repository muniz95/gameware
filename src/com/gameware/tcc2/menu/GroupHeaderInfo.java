package com.gameware.tcc2.menu;

/**
 * Created by chenduanjin on 9/13/13.
 */
public class GroupHeaderInfo {
	private String headTitle;
	private int mImg;

	public GroupHeaderInfo(String headTitle, int img) {
		this.headTitle = headTitle;
		this.mImg = img;
	}

	public String getHeadTitle() {
		return headTitle;
	}

	public void setHeadTitle(String headTitle) {
		this.headTitle = headTitle;
	}

	public int getmImg() {
		return mImg;
	}

	public void setmImg(int mImg) {
		this.mImg = mImg;
	}

}
