package com.gameware.tcc2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.gameware.tcc2.model.Console;
import com.gameware.tcc2.model.Status;
import com.gameware.tcc2.utils.CelulaREST;
import com.gameware.tcc2.utils.Constants;
import com.gameware.tcc2.utils.GamewareUtils;

public class TelaHoraConsole extends Activity {

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_tela_hora_console);
		
		List<Console> consolesList = new ArrayList<Console>();
		try {
			consolesList = CelulaREST.listaConsoles();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		List<String> consoles = new ArrayList<String>();
		
		final List<Console> consolesListAux = consolesList;
		
		for (Iterator iterator = consolesList.iterator(); iterator.hasNext();) {
			Console console = (Console) iterator.next();
			consoles.add(console.getNomeConsole());
		}
		
		final EditText textValorHora = (EditText) findViewById(R.id.text_valor_hora);
		Spinner s = (Spinner) findViewById(R.id.seletor_console);
		s.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				
			textValorHora.setText(String.valueOf(consolesListAux.get(arg2).getValorMonetario()));
			Intent it = getIntent();
			Bundle bundle = new Bundle();
			bundle.putInt(Constants.SESSION_INDICE_SELECIONADO, consolesListAux.get(arg2).getIdConsole());
			it.putExtras(bundle);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
		});
		ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, consoles);
		s.setAdapter(adapter);
        
       
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(Menu.NONE, Menu.FIRST+2, Menu.NONE, Constants.MENU_EDITAR);
		menu.add(Menu.NONE, Menu.FIRST+1, Menu.NONE, Constants.MENU_SALVAR);
		menu.add(Menu.CATEGORY_ALTERNATIVE, Menu.FIRST, Menu.NONE, Constants.MENU_EXCLUIR_CONSOLE);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		if (item.getItemId()==Menu.FIRST){
			alertaMessageConfirmExclusao(Constants.MESSAGE_CONFIRMACAO_EXCLUSAO_CONSOLE, Constants.MESSAGE_SUCESSO_EXCLUSAO_CONSOLE);
		} else if (item.getItemId()==Menu.FIRST+1){
			EditText campoValorHora = (EditText) findViewById(R.id.text_valor_hora);
			boolean valido = true;
			
			/*Valida��o de campos obrigat�rios*/
			if (GamewareUtils.campoVazio(campoValorHora)){
				Toast.makeText(TelaHoraConsole.this, "Voc� deve preencher o campo Valor por Hora", Toast.LENGTH_SHORT).show();
				valido=false;
			}
			
			if (valido){
			alertaMessageConfirmAlteracao(Constants.MESSAGE_CONFIRMACAO_ALTERACAO_CONSOLE, Constants.MESSAGE_SUCESSO_ALTERACAO_CONSOLE);
			}
		}else if (item.getItemId()==Menu.FIRST+2){
			item.setVisible(false);
			EditText campoValorHora = (EditText) findViewById(R.id.text_valor_hora);
			campoValorHora.setEnabled(true);
		}
		
		return true;
	}
	
	public void onButtonClick(View v){
	}
	
	public void alertaMessageConfirmAlteracao (String mensagemQuestion, final String mensagemSucess){

		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(mensagemQuestion)
				.setCancelable(false)
				.setPositiveButton(Constants.BUTTON_SIM,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								String mensagemSucessAux = mensagemSucess;
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TelaHoraConsole.this);
								EditText text_valor_hora = (EditText)findViewById(R.id.text_valor_hora);
								Intent it = getIntent();
								Bundle bundle = it.getExtras();
								Integer indiceSelecionado = bundle.getInt(Constants.SESSION_INDICE_SELECIONADO);
								Status status = new Status();
								try {
									status = CelulaREST.alteraConsole(indiceSelecionado, Float.valueOf(GamewareUtils.stringEditText(text_valor_hora)));
								} catch (NumberFormatException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
								if (Constants.SUCESSO_OPERACAO_BD.equals(status.getCodigoErro())){
									
								}else if (Constants.ERRO_OPERACAO_BD.equals(status.getCodigoErro())){
									alertaMessageErro(Constants.MESSAGE_ERRO_BD);
								}
									// set dialog message
									alertDialogBuilder
										.setMessage(mensagemSucess)
										.setCancelable(false)
										.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) {
												finish();
											}
										  });
						 
										// create alert dialog
										AlertDialog alertDialog = alertDialogBuilder.create();
						 
										// show it
										alertDialog.show();
							}
						})
				.setNegativeButton(Constants.BUTTON_NAO,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	
	}
	
	public void alertaMessageConfirmExclusao (String mensagemQuestion, final String mensagemSucess){

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(mensagemQuestion)
				.setCancelable(false)
				.setPositiveButton(Constants.BUTTON_SIM,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								//TODO: Chama servi�o de exclus�o
								String mensagemSucessAux = mensagemSucess;
								Intent it = getIntent();
								Bundle bundle = it.getExtras();
								Integer indiceSelecionado = bundle.getInt(Constants.SESSION_INDICE_SELECIONADO);
								boolean sucesso = true;
								if (sucesso){
									
								}else{
									mensagemSucessAux = Constants.MESSAGE_ERRO_BD;
								}
								final String mensagemAux2 = mensagemSucessAux;
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TelaHoraConsole.this);
						 
									// set dialog message
									alertDialogBuilder
										.setMessage(mensagemAux2)
										.setCancelable(false)
										.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) {
												finish();
											}
										  });
						 
										// create alert dialog
										AlertDialog alertDialog = alertDialogBuilder.create();
						 
										// show it
										alertDialog.show();
							}
						})
				.setNegativeButton(Constants.BUTTON_NAO,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		super.finish();
	}	
	
	public void alertaMessageErro(String mensagem){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// set dialog message
		alertDialogBuilder
			.setMessage(mensagem)
			.setCancelable(false)
			.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.cancel();
				}
			  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
	}
}
