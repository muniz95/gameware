package com.gameware.tcc2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

import com.gameware.tcc2.model.Cliente;
import com.gameware.tcc2.model.Equipamento;
import com.gameware.tcc2.model.FichaProduto;
import com.gameware.tcc2.model.TipoEquipamento;
import com.gameware.tcc2.utils.CelulaREST;
import com.gameware.tcc2.utils.Constants;

public class TelaSelecaoProdutoDadosCadastraisActivity extends Activity {

	@SuppressWarnings("rawtypes")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_selecao_produto);

		List<String> produtoList = new ArrayList<String>();
		List<FichaProduto> produtoWSList  = new ArrayList<FichaProduto>();
		
		
		try {
			produtoWSList = CelulaREST.listarTodosProdutos();
			for (Iterator iterator = produtoWSList.iterator(); iterator.hasNext();) {
				FichaProduto produto = (FichaProduto) iterator.next();
				produtoList.add(produto.getNomeProduto());
				
			}
		} catch (Exception e) {
			alertaMessageErro(Constants.MESSAGE_ERRO_BD);
		}
		
		final List<FichaProduto> listaProdutoResponse = produtoWSList;
		Spinner seletor_produto = (Spinner)findViewById(R.id.seletor_produto);
		seletor_produto.setOnItemSelectedListener(new OnItemSelectedListener() {

			Intent it = getIntent();
			Bundle bundle = new Bundle();
			
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				bundle.putInt(Constants.SESSION_INDICE_SELECIONADO, listaProdutoResponse.get(arg2).getIdEquipamento());
				it.putExtras(bundle);
				
				TextView text_categoria = (TextView)findViewById(R.id.text_categoria);
				TextView text_console = (TextView)findViewById(R.id.text_console);
				
				text_categoria.setText(listaProdutoResponse.get(arg2).getNomeTipo());
				text_console.setText(listaProdutoResponse.get(arg2).getNomeConsole());
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
		});
		
		@SuppressWarnings("unchecked")
		ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, produtoList);
        seletor_produto.setAdapter(adapter);
	}
	
	public void onButtonClick(View v){
		Spinner s = (Spinner) findViewById(R.id.seletor_produto);
		
		if (v.getId()==R.id.button_ok){
			finish();
			Intent it = getIntent();
			Bundle bundle = it.getExtras();
			//TODO: Verificar se o selectedItemPosition do spinner esta correto
			Intent in = new Intent(getApplicationContext(), TelaDadosCadastraisProduto.class);
			in.putExtras(bundle);
			startActivity(in);
		}
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		super.finish();
	}
	
	public void alertaMessageErro(String mensagem){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// set dialog message
		alertDialogBuilder
			.setMessage(mensagem)
			.setCancelable(false)
			.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.cancel();
				}
			  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
	}

}