package com.gameware.tcc2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.gameware.tcc2.model.Cliente;
import com.gameware.tcc2.utils.CelulaREST;
import com.gameware.tcc2.utils.Constants;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

public class TelaSelecaoClienteFichaFinanceiraActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_selecao_cliente_ficha_financeira);
		

		
		List<String> listaCliente = new ArrayList<String>();
		List<Cliente> clientesList = new ArrayList<Cliente>();
		try {
			clientesList= CelulaREST.listaClientes();
		} catch (Exception e) {
			e.printStackTrace();
		}
		final List<Cliente>listaClienteResponse = clientesList;
		
		
		//TODO: Lista de clientes
		for (Iterator iterator = listaClienteResponse.iterator(); iterator.hasNext();) {
			Cliente cliente = (Cliente) iterator.next();
			listaCliente.add(cliente.getNome());
		}
		
		
		Spinner s = (Spinner) findViewById(R.id.seletor_cliente);
		s.setOnItemSelectedListener(new OnItemSelectedListener() {

			Intent it = getIntent();
			Bundle bundle = new Bundle();
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				//TODO: Verificar se o selectedItemPosition do spinner esta correto
				bundle.putInt(Constants.SESSION_INDICE_SELECIONADO, listaClienteResponse.get(arg2).getIdCliente());
				it.putExtras(bundle);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}

		});
		
		@SuppressWarnings("unchecked")
		ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, listaCliente);
        s.setAdapter(adapter);
	}

	public void onButtonClick (View v){
		Spinner s = (Spinner) findViewById(R.id.seletor_cliente);
		if (R.id.button_ok==v.getId()){
			finish();
			Intent intent = getIntent();
			Bundle bundle = intent.getExtras();
			System.out.println(bundle.getInt(Constants.SESSION_INDICE_SELECIONADO));
			Intent in = new Intent(getApplicationContext(), TelaFichaFinanceiraClienteActivity.class);
			Bundle bundle2 = new Bundle();
			
			in.putExtras(bundle);
			startActivity(in);}
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		super.finish();
	}
}
