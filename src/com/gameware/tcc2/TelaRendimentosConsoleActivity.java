package com.gameware.tcc2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

import com.gameware.tcc2.model.RendimentosConsole;
import com.gameware.tcc2.utils.CelulaREST;
import com.gameware.tcc2.utils.Constants;
import com.gameware.tcc2.utils.GamewareUtils;

public class TelaRendimentosConsoleActivity extends ListActivity{

	public static final String TAG_CONSOLE = "consoleName";
	public static final String TAG_CONSOLE_REND = "consoleRend";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_rendimentos_console);
		ArrayList<HashMap<String, String>> consoleList = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map = new HashMap<String, String>();
		
		Intent it = getIntent();
		Bundle bundle = it.getExtras();
		
		List<RendimentosConsole> rendimentosList = new ArrayList<RendimentosConsole>();
		try {
			rendimentosList = CelulaREST.retornaRendimentosConsole(bundle.getString(Constants.SESSION_DATE_QUERY), bundle.getString(Constants.SESSION_DATE_QUERY1));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (Iterator iterator = rendimentosList.iterator(); iterator.hasNext();) {
			RendimentosConsole rendimentosConsole = (RendimentosConsole) iterator.next();
			map = new HashMap<String, String>();
			map.put(TAG_CONSOLE, rendimentosConsole.getNomeConsole());
			map.put(TAG_CONSOLE_REND, "R$"+GamewareUtils.formatValueDecimal(String.valueOf(rendimentosConsole.getValor()*10)));
			consoleList.add(map);
		}
		
		
		ListAdapter adapter = new SimpleAdapter(this, consoleList,
				R.layout.activity_tela_rendimentos_console_item,
				new String[] { TAG_CONSOLE, TAG_CONSOLE_REND}, new int[] {
				R.id.consoleName, R.id.consoleRend});
		
		setListAdapter(adapter);
	}
	}
