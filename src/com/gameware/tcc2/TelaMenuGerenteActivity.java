package com.gameware.tcc2;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.gameware.tcc2.menu.CustomArrayAdapter;
import com.gameware.tcc2.menu.DataModel;
import com.gameware.tcc2.menu.GroupHeaderInfo;
import com.gameware.tcc2.menu.RowInfo;
import com.gameware.tcc2.utils.Constants;

public class TelaMenuGerenteActivity extends ListActivity {
    private DrawerLayout mDrawerLayout;
    private ExpandableListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private CustomArrayAdapter adapter;

	// JSON Node names
	private static final String TAG_CONSOLE = "consoleName";
	private static final String TAG_CONSOLE_DESC = "consoleDesc";
	private static final String TAG_CONSOLE_IMAGE = "imageView1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_menu_gerente);

        mTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ExpandableListView) findViewById(R.id.left_drawer);


        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        adapter = new CustomArrayAdapter(initData(),this);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        //listen to child click event
        mDrawerList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                    int groupPosition, int childPosition, long id) {
            	if (groupPosition==Constants.MENU_GROUP_GER_FINANCEIRO){
            		if (childPosition==Constants.MENU_ITEM_RELATORIOS){
            			mDrawerLayout.closeDrawer(mDrawerList);
            			Intent it = new Intent(getApplicationContext(),TelaSelecaoPeriodo.class);
            			startActivity(it);
            		}
            	}else if (groupPosition==Constants.MENU_GROUP_GER_CLIENTES){
            		if (childPosition==Constants.MENU_ITEM_DADOS_CADASTRAIS_CLIENTE){
            			mDrawerLayout.closeDrawer(mDrawerList);
            			Intent it = new Intent(getApplicationContext(),TelaSelecaoClienteDadosCadastraisActivity.class);
            			startActivity(it);            			
            		}else if (childPosition==Constants.MENU_ITEM_FICHA_FINANCEIRA){
            			mDrawerLayout.closeDrawer(mDrawerList);
            			Intent it = new Intent(getApplicationContext(),TelaSelecaoClienteFichaFinanceiraActivity.class);
            			startActivity(it);            			
            		}else if (childPosition==Constants.MENU_ITEM_CADASTRO_CLIENTE){
            			mDrawerLayout.closeDrawer(mDrawerList);
            			Intent it = new Intent(getApplicationContext(),TelaCadastroClienteActivity.class);
            			startActivity(it);            			
            		}
            	}else if (groupPosition==Constants.MENU_GROUP_PRODUTOS){
            		if (childPosition==Constants.MENU_ITEM_DADOS_CADASTRAIS_PRODUTO){
            			mDrawerLayout.closeDrawer(mDrawerList);
            			Intent it = new Intent(getApplicationContext(),TelaSelecaoProdutoDadosCadastraisActivity.class);
            			startActivity(it);            			
            		}else if (childPosition==Constants.MENU_ITEM_CADASTRO_PRODUTO){
            			mDrawerLayout.closeDrawer(mDrawerList);
            			Intent it = new Intent(getApplicationContext(),TelaCadastroProduto.class);
            			startActivity(it);            			
            		}else if (childPosition==Constants.MENU_ITEM_CADASTRO_CATEGORIA){
            			mDrawerLayout.closeDrawer(mDrawerList);
            			Intent it = new Intent(getApplicationContext(),TelaCadastroCategoriaActivity.class);
            			startActivity(it);            			
            		}
            	}
				return false;
            }
        });

        mDrawerList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
            	System.out.println("entrou");
//                getActionBar().setTitle(
//                ((GroupHeaderInfo)adapter.getGroup(i)).getHeadTitle());

                return false;
            }
        });
        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
                ) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(0);
        }
        
        ArrayList<HashMap<String, String>> consoleList = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map = new HashMap<String, String>();
		
		// adding each child node to HashMap key => value
		map.put(TAG_CONSOLE, "Mega-Drive");
		map.put(TAG_CONSOLE_DESC, "Console 1");
		map.put(TAG_CONSOLE_IMAGE, String.valueOf(R.drawable.ic_console_mega_drive));
		consoleList.add(map);
		
		map = new HashMap<String, String>();
		map.put(TAG_CONSOLE, "Mega-Drive");
		map.put(TAG_CONSOLE_DESC, "Console 2");
		map.put(TAG_CONSOLE_IMAGE, String.valueOf(R.drawable.ic_console_mega_drive));
		consoleList.add(map);
		
		map = new HashMap<String, String>();
		map.put(TAG_CONSOLE, "X-Box");
		map.put(TAG_CONSOLE_DESC, "Console 1");
		map.put(TAG_CONSOLE_IMAGE, String.valueOf(R.drawable.ic_console_xbox));
		consoleList.add(map);
		
		map = new HashMap<String, String>();
		map.put(TAG_CONSOLE, "Playstation");
		map.put(TAG_CONSOLE_DESC, "Console 1");
		map.put(TAG_CONSOLE_IMAGE, String.valueOf(R.drawable.ic_console_playstation));
		consoleList.add(map);
		
		map = new HashMap<String, String>();
		map.put(TAG_CONSOLE, "Playstation");
		map.put(TAG_CONSOLE_DESC, "Console 2");
		map.put(TAG_CONSOLE_IMAGE, String.valueOf(R.drawable.ic_console_playstation));
		consoleList.add(map);
		
		map = new HashMap<String, String>();
		map.put(TAG_CONSOLE, "Nintendo");
		map.put(TAG_CONSOLE_DESC, "Console 1");
		map.put(TAG_CONSOLE_IMAGE, String.valueOf(R.drawable.ic_console_nintendo));
		consoleList.add(map);
		
		ListAdapter adapter = new SimpleAdapter(this, consoleList,
				R.layout.activity_tela_consoles_item,
				new String[] { TAG_CONSOLE, TAG_CONSOLE_DESC, "imageView1"}, new int[] {
				R.id.consoleName, R.id.consoleDesc, R.id.imageView1});
		
		setListAdapter(adapter);
		
		// selecting single ListView item
		ListView lv = getListView();
		
		// Launching new screen on Selecting Single ListItem
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
				// getting values from selected ListItem
				String consoleName = ((TextView) view.findViewById(R.id.consoleName)).getText().toString();
				// Starting new intent
				Intent in = new Intent(getApplicationContext(), TelaRegistroLocacao.class);
				in.putExtra(TAG_CONSOLE, consoleName);
				startActivity(in);
			}

		}); 
		
		// Launching new screen on Selecting Single ListItem
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View view,int position, long arg3) {
				String consoleName = ((TextView) view.findViewById(R.id.consoleName)).getText().toString();
				System.out.println(consoleName);
				String valorPagar = "50,00";
				alertMessage(valorPagar);
				return false;
			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.tela_menu_gerente, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
//        menu.findItem(R.id.action_logout).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         // The action bar home/up action should open or close the drawer.
         // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {
        case R.id.action_logout:
		    AlertDialog.Builder builder = new AlertDialog.Builder(this);
		    builder.setMessage("Voc� tem certeza que deseja sair?")
		           .setCancelable(false)
		           .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		       			finish();
		               }
		           })
		           .setNegativeButton("N�o", new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		                    dialog.cancel();
		               }
		           });
		    AlertDialog alert = builder.create();
		    alert.show();
        }
		return false;
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        	System.out.println("Posicao onItemClick: id "+id+" position "+position);
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        // update the main content by replacing fragments
    	System.out.println("Posicao selectItem: "+position);
//        FragmentManager fragmentManager = getFragmentManager();
//        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
//        setTitle(mPlanetTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
    	title = "Gameware";
        mTitle = title;
        getActionBar().setTitle("Gameware");
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private DataModel initData()
    {
        ArrayList<GroupHeaderInfo> headArray = new ArrayList<GroupHeaderInfo>();
        ArrayList<ArrayList<RowInfo>> rowsArray = new ArrayList<ArrayList<RowInfo>>();

        //Asian country
        {
            GroupHeaderInfo info = new GroupHeaderInfo("Gerenciamento financeiro da loja", R.drawable.ic_clients);
            headArray.add(info);
            RowInfo row = new RowInfo("Relat�rios");
            ArrayList<RowInfo> rowArray = new ArrayList<RowInfo>();
            rowArray.add(row);
            rowsArray.add(rowArray);
        }

        {
            GroupHeaderInfo info2 = new GroupHeaderInfo("Gerenciamento de clientes", R.drawable.ic_console_mega_drive);
            headArray.add(info2);

            ArrayList<RowInfo> rowArray = new ArrayList<RowInfo>();
            RowInfo row = new RowInfo("Dados cadastrais");
            rowArray.add(row);
            RowInfo row2 = new RowInfo("Ficha financeira");
            rowArray.add(row2);
            RowInfo row3 = new RowInfo("Cadastrar novo cliente");
            rowArray.add(row3);
            rowsArray.add(rowArray);
        }

        {
            GroupHeaderInfo info3 = new GroupHeaderInfo("Produtos", R.drawable.ic_console_nintendo);
            headArray.add(info3);

            ArrayList<RowInfo> rowArray = new ArrayList<RowInfo>();
            RowInfo row = new RowInfo("Dados cadastrais");
            rowArray.add(row);
            RowInfo row2 = new RowInfo("Cadastrar novo produto");
            rowArray.add(row2);
            RowInfo row3 = new RowInfo("Cadastrar categoria");
            rowArray.add(row3);
            rowsArray.add(rowArray);

        }

        return new DataModel(headArray,rowsArray);
    }
    
	public void alertMessage(final String valorPagar) {
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE: // Yes button clicked
					
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							TelaMenuGerenteActivity.this);
			 
						// set dialog message
						alertDialogBuilder
							.setTitle("Valor a pagar")
							.setMessage("R$"+valorPagar)
							.setCancelable(false)
							.setNeutralButton("OK",new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {
									// if this button is clicked, close
									// current activity
								}
							  });
			 
							// create alert dialog
							AlertDialog alertDialog = alertDialogBuilder.create();
			 
							// show it
							alertDialog.show();
					
					
					break;
				case DialogInterface.BUTTON_NEGATIVE: // No button clicked // do
					break;
				}
			}
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Deseja realmente encerrar esta sess�o?").setPositiveButton("Sim", dialogClickListener).setNegativeButton("N�o", dialogClickListener).show();
		}
		@Override
		public void onDestroy(){
			super.onDestroy();
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
		    builder.setMessage("Voc� tem certeza que deseja sair?")
		           .setCancelable(false)
		           .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		       			finish();
		               }
		           })
		           .setNegativeButton("N�o", new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		                    dialog.cancel();
		               }
		           });
		    AlertDialog alert = builder.create();
		    alert.show();
		}
}