package com.gameware.tcc2;

import com.gameware.tcc2.utils.Constants;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class TelaGerenciarConsole extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_gerenciar_console);
	}

	public void onButtonClick(View v){
		if (R.id.button_valor_utilizacao==v.getId()){
			Bundle bundle = new Bundle();
			bundle.putInt(Constants.SESSION_MANUTENCAO_CONSOLE, Constants.ALTER_CONSOLE);
			Intent intent = new Intent(TelaGerenciarConsole.this,TelaHoraConsole.class);
			intent.putExtras(bundle);
			startActivity(intent);
		}else if (R.id.button_cadastrar_console==v.getId()){
			Bundle bundle = new Bundle();
			bundle.putInt(Constants.SESSION_MANUTENCAO_CONSOLE, Constants.CADASTRO_CONSOLE);
			Intent intent = new Intent(TelaGerenciarConsole.this,TelaHoraConsole.class);
			intent.putExtras(bundle);
			startActivity(intent);
		}else if (R.id.button_ger_desconto==v.getId()){
			Bundle bundle = new Bundle();
			bundle.putInt(Constants.SESSION_MANUTENCAO_DESCONTO, Constants.ALTER_DESCONTO);
			Intent intent = new Intent(TelaGerenciarConsole.this,TelaDescontoActivity.class);
			intent.putExtras(bundle);
			startActivity(intent);
		}else if (R.id.button_cadastrar_desconto==v.getId()){
			Bundle bundle = new Bundle();
			bundle.putInt(Constants.SESSION_MANUTENCAO_DESCONTO, Constants.CADASTRO_DESCONTO);
			Intent intent = new Intent(TelaGerenciarConsole.this,TelaDescontoActivity.class);
			intent.putExtras(bundle);
			startActivity(intent);
		}
	}
}
