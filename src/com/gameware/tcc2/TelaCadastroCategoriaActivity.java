package com.gameware.tcc2;

import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.gameware.tcc2.model.Status;
import com.gameware.tcc2.utils.CelulaREST;
import com.gameware.tcc2.utils.Constants;
import com.gameware.tcc2.utils.GamewareUtils;

public class TelaCadastroCategoriaActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_cadastro_categoria);
	}

	public void onButtonClick(View v){
		EditText textNomeCategoria = (EditText) findViewById(R.id.text_nome_categoria);
		boolean valido = true;
		if (R.id.button_ok==v.getId()){
			if (Constants.EMPTY.equals(textNomeCategoria.getText().toString())){
				Toast.makeText(TelaCadastroCategoriaActivity.this, "Voc� deve preencher o campo Nome da Categoria", Toast.LENGTH_SHORT).show();
				valido = false;
			}
			
			if (valido) {
				Status status = new Status();
				try {
					status = CelulaREST.insereCategoria(GamewareUtils.stringEditText(textNomeCategoria));
				} catch (JSONException e) {
					status.setCodigoErro(Constants.ERRO_OPERACAO_BD);
					e.printStackTrace();
				}

				if (Constants.SUCESSO_OPERACAO_BD.equals(status.getCodigoErro())) {
					alertaMessageOk(Constants.MESSAGE_SUCESSO_CADASTRO_CATEGORIA);
				} else if (Constants.ERRO_CATEGORIA_EXISTENTE.equals(status.getCodigoErro())) {
					alertaMessageErro(Constants.MESSAGE_ERRO_CADASTRO_CATEGORIA);
				} else if (Constants.ERRO_OPERACAO_BD.equals(status.getCodigoErro())) {
					alertaMessageErro(Constants.MESSAGE_ERRO_BD);
				}
			}
		}
	}
	
	public void alertaMessageOk(String mensagem){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// set dialog message
		alertDialogBuilder
			.setMessage(mensagem)
			.setCancelable(false)
			.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					finish();
				}
			  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
	}
	
	public void alertaMessageErro(String mensagem){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		// set dialog message
		alertDialogBuilder
			.setMessage(mensagem)
			.setCancelable(false)
			.setNeutralButton(Constants.BUTTON_OK,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.cancel();
				}
			  });

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		super.finish();
	}
}
