package com.gameware.tcc2.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gameware.tcc2.model.CadastroCliente;
import com.gameware.tcc2.model.Cliente;
import com.gameware.tcc2.model.Console;
import com.gameware.tcc2.model.Desconto;
import com.gameware.tcc2.model.FichaCliente;
import com.gameware.tcc2.model.FichaProduto;
import com.gameware.tcc2.model.Locacao;
import com.gameware.tcc2.model.RendimentosCliente;
import com.gameware.tcc2.model.RendimentosConsole;
import com.gameware.tcc2.model.Status;
import com.gameware.tcc2.model.TipoEquipamento;
import com.gameware.tcc2.model.Usuario;


/**
* 
* Método responsável por fazer chamada ao web service e buscar as
* informações(json)atraves da URI.
* 
* @return ArrayList<Usuario>
* @throws Exception
* @author Douglas Costa <douglas.cst90@gmail.com.br>
* @since 06/06/2013 10:29:03
* @version 1.0
*/
public class CelulaREST {
	private static String URI = "http://10.0.2.2:8080"; 
	
	public static List<Cliente> listaClientes() throws Exception {
	
		String[] json = new WebService().get(URI + "/Restful/usuario/listarTodosClientes");
		
		List<Cliente> usuarios = new ArrayList<Cliente>();
		
		if (json[0].equals("200")) {
		
		Cliente cadastroCliente = new Cliente();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		if (!json[1].contains("[")){
			JSONObject response = new JSONObject(json[1]); 
			cadastroCliente.setIdCliente(response.getJSONObject("cliente").getInt("idCliente"));
			cadastroCliente.setNome(response.getJSONObject("cliente").getString("nome"));
			cadastroCliente.setIdade(response.getJSONObject("cliente").getInt("idade"));
			cadastroCliente.setSexo(response.getJSONObject("cliente").getString("sexo"));
			cadastroCliente.setDataNascimento(sdf.parse(response.getJSONObject("cliente").getString("dataNascimento").replace("T"," ")));
			cadastroCliente.setRg(response.getJSONObject("cliente").getInt("rg"));
			cadastroCliente.setCpf(response.getJSONObject("cliente").getLong("cpf"));
			cadastroCliente.setEndRua(response.getJSONObject("cliente").getString("endRua"));
			cadastroCliente.setEndNum(response.getJSONObject("cliente").getString("endNum"));
			try {
				cadastroCliente.setEndCompl(response.getJSONObject("cliente").getString("endCompl"));
			} catch (Exception e) {
				cadastroCliente.setEndCompl(Constants.EMPTY);
			}
			cadastroCliente.setEndBairro(response.getJSONObject("cliente").getString("endBairro"));
			cadastroCliente.setEndCep(response.getJSONObject("cliente").getString("endCep"));
			cadastroCliente.setEndCidade(response.getJSONObject("cliente").getString("endCidade"));
			cadastroCliente.setEndEstado(response.getJSONObject("cliente").getString("endEstado"));	
			usuarios.add(cadastroCliente);
		}else{
			JSONObject response = new JSONObject(json[1]);
			JSONArray responseArray = response.getJSONArray("cliente");
			for (int i = 0; i < responseArray.length(); i++) {
				JSONObject responseObject = (JSONObject) responseArray.get(i);
				cadastroCliente = new Cliente();
				cadastroCliente.setIdCliente(responseObject.getInt("idCliente"));
				cadastroCliente.setNome(responseObject.getString("nome"));
				cadastroCliente.setIdade(responseObject.getInt("idade"));
				cadastroCliente.setSexo(responseObject.getString("sexo"));
				cadastroCliente.setDataNascimento(sdf.parse(responseObject.getString("dataNascimento").replace("T"," ")));
				cadastroCliente.setRg(responseObject.getInt("rg"));
				cadastroCliente.setCpf(responseObject.getLong("cpf"));
				cadastroCliente.setEndRua(responseObject.getString("endRua"));
				cadastroCliente.setEndNum(responseObject.getString("endNum"));
				try {
					cadastroCliente.setEndCompl(responseObject.getString("endCompl"));
				} catch (Exception e) {
					cadastroCliente.setEndCompl(Constants.EMPTY);
				}
				cadastroCliente.setEndBairro(responseObject.getString("endBairro"));
				cadastroCliente.setEndCep(responseObject.getString("endCep"));
				cadastroCliente.setEndCidade(responseObject.getString("endCidade"));
				cadastroCliente.setEndEstado(responseObject.getString("endEstado"));	
				usuarios.add(cadastroCliente);
			}
		}
		return usuarios;
			
		}
		else {
			throw new Exception(json[1]);
		}	
	}
	
	public static List<Cliente> listaGerentes() throws Exception {
		
		String[] json = new WebService().get(URI + "/Restful/usuario/listarTodosGerentes");
		
		List<Cliente> usuarios = new ArrayList<Cliente>();
		
		if (json[0].equals("200")) {
		
		Cliente cadastroCliente = new Cliente();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		if (!json[1].contains("[")){
			JSONObject response = new JSONObject(json[1]); 
			cadastroCliente.setIdCliente(response.getJSONObject("cliente").getInt("idCliente"));
			cadastroCliente.setNome(response.getJSONObject("cliente").getString("nome"));
			cadastroCliente.setIdade(response.getJSONObject("cliente").getInt("idade"));
			cadastroCliente.setSexo(response.getJSONObject("cliente").getString("sexo"));
			cadastroCliente.setDataNascimento(sdf.parse(response.getJSONObject("cliente").getString("dataNascimento").replace("T"," ")));
			cadastroCliente.setRg(response.getJSONObject("cliente").getInt("rg"));
			cadastroCliente.setCpf(response.getJSONObject("cliente").getLong("cpf"));
			cadastroCliente.setEndRua(response.getJSONObject("cliente").getString("endRua"));
			cadastroCliente.setEndNum(response.getJSONObject("cliente").getString("endNum"));
			cadastroCliente.setIdUsuario(response.getJSONObject("cliente").getInt("idUsuario"));
			try {
				cadastroCliente.setEndCompl(response.getJSONObject("cliente").getString("endCompl"));
			} catch (Exception e) {
				cadastroCliente.setEndCompl(Constants.EMPTY);
			}
			cadastroCliente.setEndBairro(response.getJSONObject("cliente").getString("endBairro"));
			cadastroCliente.setEndCep(response.getJSONObject("cliente").getString("endCep"));
			cadastroCliente.setEndCidade(response.getJSONObject("cliente").getString("endCidade"));
			cadastroCliente.setEndEstado(response.getJSONObject("cliente").getString("endEstado"));	
			usuarios.add(cadastroCliente);
		}else{
			JSONObject response = new JSONObject(json[1]);
			JSONArray responseArray = response.getJSONArray("cliente");
			for (int i = 0; i < responseArray.length(); i++) {
				JSONObject responseObject = (JSONObject) responseArray.get(i);
				cadastroCliente = new Cliente();
				cadastroCliente.setIdCliente(responseObject.getInt("idCliente"));
				cadastroCliente.setNome(responseObject.getString("nome"));
				cadastroCliente.setIdade(responseObject.getInt("idade"));
				cadastroCliente.setSexo(responseObject.getString("sexo"));
				cadastroCliente.setDataNascimento(sdf.parse(responseObject.getString("dataNascimento").replace("T"," ")));
				cadastroCliente.setRg(responseObject.getInt("rg"));
				cadastroCliente.setCpf(responseObject.getLong("cpf"));
				cadastroCliente.setEndRua(responseObject.getString("endRua"));
				cadastroCliente.setEndNum(responseObject.getString("endNum"));
				cadastroCliente.setIdUsuario(responseObject.getInt("idUsuario"));
				try {
					cadastroCliente.setEndCompl(responseObject.getString("endCompl"));
				} catch (Exception e) {
					cadastroCliente.setEndCompl(Constants.EMPTY);
				}
				cadastroCliente.setEndBairro(responseObject.getString("endBairro"));
				cadastroCliente.setEndCep(responseObject.getString("endCep"));
				cadastroCliente.setEndCidade(responseObject.getString("endCidade"));
				cadastroCliente.setEndEstado(responseObject.getString("endEstado"));	
				usuarios.add(cadastroCliente);
			}
		}
		return usuarios;
			
		}
		else {
			throw new Exception(json[1]);
		}	
	}
	
	public static Status alteraCliente(String idCliente, String nome, String sexo, String dataNascimento, Integer rg, String cpf, String endRua, String endNum, String endCompl, String endBairro, String endCep, String endCidade, String endEstado, String email, String telefone, String celular, String skype, String facebook, String gtalk, String whatsapp) throws Exception{
		String parametros = "/Restful/usuario/alteraCliente";
		Status status = new Status();
		parametros += "/"+idCliente+"/"+nome+"/"+sexo+"/"+MaskUtils.unmask(dataNascimento)+"/"+rg+"/"+cpf+"/"+endRua+"/"+endNum+"/"+endCompl+"/"+endBairro+"/"+endCep+"/"+endCidade+"/"+endEstado+"/"+email+"/"+telefone+"/"+celular+"/"+skype+"/"+facebook+"/"+gtalk+"/"+whatsapp;
		parametros = parametros.replace(" ","%20");
		String[] json = new WebService().get(URI +parametros);
		JSONObject response = new JSONObject(json[1]);
		status.setCodigoErro(response.getInt("codigoErro"));
		return status;
	}
	
	public static Status redefinirSenhaGerente(Integer idGerente, String novaSenha) throws JSONException{
		String parametros = "/Restful/usuario/redefinirSenhaGerente";
		Status status = new Status();
		parametros += "/"+idGerente+"/"+novaSenha;
		parametros = parametros.replace(" ","%20");
		String[] json = null;
		try {
			json = new WebService().get(URI +parametros);
		} catch (Exception e) {
			status.setCodigoErro(0);
			e.printStackTrace();
		}
		JSONObject response = new JSONObject(json[1]);
		status.setCodigoErro(response.getInt("codigoErro"));
		return status;
	}
	
	public static Status cadastraCliente(String nome, String sexo, String dataNascimento, Integer rg, String cpf, String endRua, String endNum, String endCompl, String endBairro, String endCep, String endCidade, String endEstado, String email, String telefone, String celular, String skype, String facebook, String gtalk, String whatsapp) throws Exception{
		String parametros = "/Restful/usuario/cadastraCliente";
		parametros += "/"+nome+"/"+sexo+"/"+MaskUtils.unmask(dataNascimento)+"/"+rg+"/"+cpf+"/"+endRua+"/"+endNum+"/"+endCompl+"/"+endBairro+"/"+endCep+"/"+endCidade+"/"+endEstado+"/"+email+"/"+telefone+"/"+celular+"/"+skype+"/"+facebook+"/"+gtalk+"/"+whatsapp;
		parametros = parametros.replace(" ","%20");
		String[] json = new WebService().get(URI +parametros);
		Status status = new Status();
		JSONObject response = new JSONObject(json[1]);
		status.setCodigoErro(response.getInt("codigoErro"));
		
		return status;
	}
	
	public static Status cadastraGerente(String nome, String sexo, String dataNascimento, Integer rg, String cpf, String endRua, String endNum, String endCompl, String endBairro, String endCep, String endCidade, String endEstado, String email, String telefone, String celular, String skype, String facebook, String gtalk, String whatsapp) throws Exception{
		String parametros = "/Restful/usuario/cadastraGerente";
		parametros += "/"+nome+"/"+sexo+"/"+MaskUtils.unmask(dataNascimento)+"/"+rg+"/"+cpf+"/"+endRua+"/"+endNum+"/"+endCompl+"/"+endBairro+"/"+endCep+"/"+endCidade+"/"+endEstado+"/"+email+"/"+telefone+"/"+celular+"/"+skype+"/"+facebook+"/"+gtalk+"/"+whatsapp;
		parametros = parametros.replace(" ","%20");
		String[] json = new WebService().get(URI +parametros);
		Status status = new Status();
		JSONObject response = new JSONObject(json[1]);
		status.setCodigoErro(response.getInt("codigoErro"));
		
		return status;
	}
	
	public static Status alteraConsole(Integer idConsole, Float valorHora) throws Exception{
		String parametros = "/Restful/usuario/alteraConsole";
		parametros += "/"+idConsole+"/"+valorHora;
		parametros = parametros.replace(" ","%20");
		String[] json = new WebService().get(URI +parametros);
		Status status = new Status();
		JSONObject response = new JSONObject(json[1]);
		status.setCodigoErro(response.getInt("codigoErro"));
		return status;
	}
	
	public static Status alteraProduto(Integer idProduto, String nomeProduto, Integer qtdProduto, Float valorProduto, boolean isConsole) throws JSONException{
		String parametros = "/Restful/usuario/alteraProduto";
		parametros += "/"+idProduto+"/"+nomeProduto+"/"+qtdProduto+"/"+valorProduto+"/"+isConsole;
		parametros = parametros.replace(" ","%20");
		String[] json = null;
		Status status = new Status();
		try {
			json = new WebService().get(URI +parametros);
		} catch (Exception e) {
			status.setCodigoErro(Constants.ERRO_OPERACAO_BD);
			e.printStackTrace();
		}
		JSONObject response = new JSONObject(json[1]);
		status.setCodigoErro(response.getInt("codigoErro"));
		
		return status;
	}
	
	public static Locacao retornaFaturamentoTotal(String dtIni, String dtFim) throws JSONException{
		String parametros = "/Restful/usuario/retornaFaturamentoTotal";
		dtIni = dtIni.replace("/","");
		dtFim = dtFim.replace("/","");
		parametros += "/"+dtIni+"/"+dtFim;
		parametros = parametros.replace(" ","%20");
		String[] json = null;
		Locacao locacao = new Locacao();
		try {
			json = new WebService().get(URI +parametros);
		} catch (Exception e) {
			e.printStackTrace();
		}
		JSONObject response = new JSONObject(json[1]);
		locacao.setValorGasto(Float.valueOf(response.getString("valorGasto")));
		
		return locacao;
	}
	
	public static ArrayList<RendimentosCliente> retornaRendimentosCliente(String dtIni, String dtFim) throws JSONException{
		String parametros = "/Restful/usuario/retornaRendimentosCliente";
		dtIni = dtIni.replace("/","");
		dtFim = dtFim.replace("/","");
		parametros += "/"+dtIni+"/"+dtFim;
		parametros = parametros.replace(" ","%20");
		String[] json = null;
		Locacao locacao = new Locacao();
		try {
			json = new WebService().get(URI +parametros);
		} catch (Exception e) {
			e.printStackTrace();
		}
		JSONObject response = new JSONObject(json[1]);
		
		RendimentosCliente rendimentosCliente = new RendimentosCliente();
		ArrayList<RendimentosCliente> rendimentosList = new ArrayList<RendimentosCliente>();
		
		if (!json[1].contains("[")){
			rendimentosCliente.setNomeCliente(response.getJSONObject("rendimentosCliente").getString("nomeCliente"));
			rendimentosCliente.setValorGasto(Float.valueOf(response.getJSONObject("rendimentosCliente").getString("valorGasto")));
			rendimentosList.add(rendimentosCliente);
		}else{
			JSONArray responseArray = response.getJSONArray("rendimentosCliente");
			for (int i = 0; i < responseArray.length(); i++) {
				JSONObject responseObject = (JSONObject) responseArray.get(i);
				rendimentosCliente = new RendimentosCliente();
				rendimentosCliente.setNomeCliente(responseObject.getString("nomeCliente"));
				rendimentosCliente.setValorGasto(Float.valueOf(responseObject.getString("valorGasto")));
				rendimentosList.add(rendimentosCliente);
			}
		}
		
		return rendimentosList;
	}
	
	public static ArrayList<RendimentosConsole> retornaRendimentosConsole(String dtIni, String dtFim) throws JSONException{
		String parametros = "/Restful/usuario/retornaRendimentosConsole";
		dtIni = dtIni.replace("/","");
		dtFim = dtFim.replace("/","");
		parametros += "/"+dtIni+"/"+dtFim;
		parametros = parametros.replace(" ","%20");
		String[] json = null;
		try {
			json = new WebService().get(URI +parametros);
		} catch (Exception e) {
			e.printStackTrace();
		}
		JSONObject response = new JSONObject(json[1]);
		
		RendimentosConsole rendimentosConsole = new RendimentosConsole();
		ArrayList<RendimentosConsole> rendimentosList = new ArrayList<RendimentosConsole>();
		
		if (!json[1].contains("[")){
			rendimentosConsole.setNomeConsole(response.getJSONObject("rendimentosConsole").getString("nomeConsole"));
			rendimentosConsole.setValor(Float.valueOf(response.getJSONObject("rendimentosConsole").getString("valor")));
			rendimentosList.add(rendimentosConsole);
		}else{
			JSONArray responseArray = response.getJSONArray("rendimentosConsole");
			for (int i = 0; i < responseArray.length(); i++) {
				JSONObject responseObject = (JSONObject) responseArray.get(i);
				rendimentosConsole = new RendimentosConsole();
				rendimentosConsole.setNomeConsole(responseObject.getString("nomeConsole"));
				rendimentosConsole.setValor(Float.valueOf(responseObject.getString("valor")));
				rendimentosList.add(rendimentosConsole);
			}
		}
		
		return rendimentosList;
	}
	
	public static Status cadastraProduto(String nome, Integer idCategoria, String idConsole, Integer qtdEstoque, Float valorMonetario, boolean isConsole, boolean naoAssociado) throws JSONException{
		String parametros = "/Restful/usuario/cadastraProduto";
		if (naoAssociado){
			idConsole="a";
		}
		parametros += "/"+nome+"/"+idCategoria+"/"+idConsole+"/"+qtdEstoque+"/"+valorMonetario+"/"+isConsole;
		parametros = parametros.replace(" ","%20");
		String[] json = null;
		Status status = new Status();
		try {
			json = new WebService().get(URI +parametros);
		} catch (Exception e) {
			status.setCodigoErro(Constants.ERRO_OPERACAO_BD);
			e.printStackTrace();
		}
		JSONObject response = new JSONObject(json[1]);
		status.setCodigoErro(response.getInt("codigoErro"));
		
		return status;
	}
	
	
	public static Status insereCategoria(String categoria) throws JSONException{
		String parametros = "/Restful/usuario/insereCategoria/"+categoria.trim();
		String[] json = null;
		Status status = new Status();
		try {
			json = new WebService().get(URI +parametros);
		} catch (Exception e) {
			status.setCodigoErro(Constants.ERRO_OPERACAO_BD);
			e.printStackTrace();
		}
		JSONObject response = new JSONObject(json[1]);
		status.setCodigoErro(response.getInt("codigoErro"));
		
		return status;
	}
	
	public static Status insereConsole(String nomeConsole, Float valorHora) throws JSONException{
		String parametros = "/Restful/usuario/insereConsole";
		parametros += "/"+nomeConsole+"/"+valorHora;
		parametros = parametros.replace(" ","%20");
		String[] json = null;
		Status status = new Status();
		try {
			json = new WebService().get(URI +parametros);
		} catch (Exception e) {
			status.setCodigoErro(Constants.ERRO_OPERACAO_BD);
			e.printStackTrace();
		}
		JSONObject response = new JSONObject(json[1]);
		status.setCodigoErro(response.getInt("codigoErro"));
		
		return status;
	}
	
	public static Status excluiCliente(Integer idCliente) throws Exception{
		String parametros = "/Restful/usuario/excluiCliente/";
		parametros += idCliente;
		String[] json = new WebService().get(URI +parametros);
		Status status = new Status();
		JSONObject response = new JSONObject(json[1]);
		status.setCodigoErro(response.getInt("codigoErro"));
		
		return status;
	}
	
	public static CadastroCliente retornaCliente(Integer idCliente) throws Exception {
		
		String parametros = "/Restful/usuario/retornaCliente";
		parametros += "/"+idCliente;
		String[] json = new WebService().get(URI +parametros);
		
		if (json[0].equals("200")) {
		
		CadastroCliente cadastroCliente = new CadastroCliente();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
			JSONObject response = new JSONObject(json[1]); 
			cadastroCliente.setIdCliente(response.getInt("idCliente"));
			cadastroCliente.setNome(response.getString("nome"));
			cadastroCliente.setIdade(response.getInt("idade"));
			cadastroCliente.setSexo(response.getString("sexo"));
			cadastroCliente.setDataNascimento(sdf.parse(response.getString("dataNascimento").replace("T"," ")));
			cadastroCliente.setRg(response.getInt("rg"));
			cadastroCliente.setCpf(response.getString("cpf"));
			cadastroCliente.setEndRua(response.getString("endRua"));
			cadastroCliente.setEndNum(response.getString("endNum"));
			try {
				cadastroCliente.setEndCompl(response.getString("endCompl"));
			} catch (Exception e) {
				cadastroCliente.setEndCompl(Constants.EMPTY);
			}
			cadastroCliente.setEndBairro(response.getString("endBairro"));
			cadastroCliente.setEndCep(response.getString("endCep"));
			cadastroCliente.setEndCidade(response.getString("endCidade"));
			cadastroCliente.setEndEstado(response.getString("endEstado"));
			cadastroCliente.setEmail(response.getString("email"));
			cadastroCliente.setTelefone(response.getString("telefone"));
			try{
				cadastroCliente.setTelefone_cel(response.getString("celular"));
			} catch (Exception e){
				cadastroCliente.setTelefone_cel("");
			}
			try{
				cadastroCliente.setSkype(response.getString("skype"));
			}catch(Exception e){
				cadastroCliente.setSkype("");
			}
			try{
				cadastroCliente.setFacebook(response.getString("facebook"));
			}catch(Exception e){
				cadastroCliente.setFacebook("");
			}
			try{
				cadastroCliente.setGtalk(response.getString("gtalk"));
			}catch(Exception e){
				cadastroCliente.setGtalk("");
			}
			try{
				cadastroCliente.setWhatsapp(response.getString("whatsapp"));
			}catch(Exception e){
				cadastroCliente.setWhatsapp("");
			}
			
		return cadastroCliente;
			
		}
		else {
			throw new Exception(json[1]);
		}	
	}

	public static FichaProduto retornaProduto(Integer idProduto) throws Exception {
		
		String parametros = "/Restful/usuario/retornaProduto";
		parametros += "/"+idProduto;
		String[] json = new WebService().get(URI +parametros);
		
		if (json[0].equals("200")) {
		
		FichaProduto fichaProduto = new FichaProduto();
		
		JSONObject response = new JSONObject(json[1]); 
		fichaProduto.setIdEquipamento(response.getInt("idEquipamento"));
		fichaProduto.setNomeProduto(response.getString("nomeProduto"));
		fichaProduto.setNomeConsole(response.getString("nomeConsole"));
		fichaProduto.setNomeTipo(response.getString("nomeTipo"));
		fichaProduto.setQuantidadeEstoque(response.getInt("quantidadeEstoque"));
		fichaProduto.setAssociadoConsole(response.getBoolean("associadoConsole"));
		fichaProduto.setValorMonetario(Float.valueOf(response.getString("valorMonetario")));
			
		return fichaProduto;

		} else {
			throw new Exception(json[1]);
		}
	}
	
	public Usuario login(String login, String senha) throws Exception{
		String parametros = "/Restful/usuario/login";
		senha = GamewareUtils.stringToBytesMD5(senha);
		parametros += "/"+login+"/"+senha;
		String[] json = new WebService().get(URI +parametros);
		
		if (json[0].equals("200")) {
			
			Usuario usuario = new Usuario();
			

			JSONObject response = new JSONObject(json[1]); 
			
			usuario.setIdUsuario(response.getInt("idUsuario"));
			if (usuario.getIdUsuario()!=Constants.WS_SEM_REGISTROS){
				usuario.setLogin(response.getString("login"));
				usuario.setSenha(response.getString("senha"));
				usuario.setTipoUsuario(response.getString("tipoUsuario"));
			}
			return usuario;
				
			}
			else {
				throw new Exception(json[1]);
			}
	}

	public static List<FichaCliente> retornaFichaCliente(Integer idCliente) throws Exception {
		
		// EX: 200.153.36.140:8070
		// Array de String que recebe o JSON do Web Service
		String[] json = new WebService().get(URI + "/Restful/usuario/fichaCliente/"+idCliente);
		
		List<FichaCliente> fichaClienteList= new ArrayList<FichaCliente>();
		
		if (json[0].equals("200")) {
		
		FichaCliente fichaCliente = new FichaCliente();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		if (!json[1].contains("[")){
			JSONObject response = new JSONObject(json[1]); 
			
			fichaCliente.setNomeConsole(response.getJSONObject("fichaCliente").getString("nomeConsole"));
			fichaCliente.setInicioLocacao(sdf.parse(response.getJSONObject("fichaCliente").getString("inicioLocacao").replace("T"," ")));
			fichaCliente.setFimLocacao(sdf.parse(response.getJSONObject("fichaCliente").getString("fimLocacao").replace("T"," ")));
			fichaCliente.setValorGasto(Float.valueOf(response.getJSONObject("fichaCliente").getString("valorGasto")));
			fichaClienteList.add(fichaCliente);
		}else{
			JSONObject response = new JSONObject(json[1]);
			JSONArray responseArray = response.getJSONArray("fichaCliente");
			for (int i = 0; i < responseArray.length(); i++) {
				JSONObject responseObject = (JSONObject) responseArray.get(i);
				fichaCliente = new FichaCliente();
				fichaCliente.setNomeConsole(responseObject.getString("nomeConsole"));
				fichaCliente.setInicioLocacao(sdf.parse(responseObject.getString("inicioLocacao").replace("T"," ")));
				fichaCliente.setFimLocacao(sdf.parse(responseObject.getString("fimLocacao").replace("T"," ")));
				fichaCliente.setValorGasto(Float.valueOf(responseObject.getString("valorGasto")));
				fichaClienteList.add(fichaCliente);
			}
		}
		
		return fichaClienteList;
			
		}
		else {
			throw new Exception(json[1]);
		}	
	}
	
	public static List<TipoEquipamento> listaCategorias() throws Exception {
		
		String[] json = new WebService().get(URI + "/Restful/usuario/listarTodasCategorias");
		
		List<TipoEquipamento> categorias = new ArrayList<TipoEquipamento>();
		
		if (json[0].equals("200")) {
		
		TipoEquipamento categoria = new TipoEquipamento();
		
		if (!json[1].contains("[")){
			JSONObject response = new JSONObject(json[1]); 
			categoria.setIdTipo(response.getJSONObject("tipoEquipamento").getInt("idTipo"));
			categoria.setNomeTipo(response.getJSONObject("tipoEquipamento").getString("nomeTipo"));
			categorias.add(categoria);
		}else{
			JSONObject response = new JSONObject(json[1]);
			JSONArray responseArray = response.getJSONArray("tipoEquipamento");
			for (int i = 0; i < responseArray.length(); i++) {
				JSONObject responseObject = (JSONObject) responseArray.get(i);
				categoria = new TipoEquipamento();
				categoria.setIdTipo(responseObject.getInt("idTipo"));
				categoria.setNomeTipo(responseObject.getString("nomeTipo"));
				categorias.add(categoria);
			}
		}
		return categorias;
			
		}
		else {
			throw new Exception(json[1]);
		}	
	}
	
	public static List<FichaProduto> listarTodosProdutos() throws Exception {
		
		String[] json = new WebService().get(URI + "/Restful/usuario/listarTodosProdutos");
		
		List<FichaProduto> produtos = new ArrayList<FichaProduto>();
		
		if (json[0].equals("200")) {
		
		FichaProduto produto = new FichaProduto();
		
		if (!json[1].contains("[")){
			JSONObject response = new JSONObject(json[1]); 
			produto.setIdEquipamento(response.getJSONObject("fichaProduto").getInt("idEquipamento"));
			produto.setNomeProduto(response.getJSONObject("fichaProduto").getString("nomeProduto"));
			produto.setNomeConsole(response.getJSONObject("fichaProduto").getString("nomeConsole"));
			produto.setNomeTipo(response.getJSONObject("fichaProduto").getString("nomeTipo"));
			produtos.add(produto);
		}else{
			JSONObject response = new JSONObject(json[1]);
			JSONArray responseArray = response.getJSONArray("fichaProduto");
			for (int i = 0; i < responseArray.length(); i++) {
				JSONObject responseObject = (JSONObject) responseArray.get(i);
				produto = new FichaProduto();
				produto.setIdEquipamento(responseObject.getInt("idEquipamento"));
				produto.setNomeProduto(responseObject.getString("nomeProduto"));
				produto.setNomeConsole(responseObject.getString("nomeConsole"));
				produto.setNomeTipo(responseObject.getString("nomeTipo"));
				produtos.add(produto);
			}
		}
		return produtos;
			
		}
		else {
			throw new Exception(json[1]);
		}	
	}
	
	public static List<Desconto> listarTodosDescontos() throws Exception {
		
		String[] json = new WebService().get(URI + "/Restful/usuario/listarTodosDescontos");
		
		List<Desconto> descontos = new ArrayList<Desconto>();
		
		if (json[0].equals("200")) {
		
		Desconto desconto = new Desconto();
		
		if (!json[1].contains("[")){
			JSONObject response = new JSONObject(json[1]);
			desconto = new Desconto();
			desconto.setIdDesconto(response.getJSONObject("desconto").getInt("idDesconto"));
			desconto.setNomeDesconto(response.getJSONObject("desconto").getString("nomeDesconto"));
			descontos.add(desconto);
		}else{
			JSONObject response = new JSONObject(json[1]);
			JSONArray responseArray = response.getJSONArray("desconto");
			for (int i = 0; i < responseArray.length(); i++) {
				JSONObject responseObject = (JSONObject) responseArray.get(i);
				desconto = new Desconto();
				desconto.setIdDesconto(responseObject.getInt("idDesconto"));
				desconto.setNomeDesconto(responseObject.getString("nomeDesconto"));
				descontos.add(desconto);
			}
		}
		return descontos;
			
		}
		else {
			throw new Exception(json[1]);
		}	
	}
	
	public static List<Console> listaConsoles() throws Exception {
			
			String[] json = new WebService().get(URI + "/Restful/usuario/listarTodosConsoles");
			
			List<Console> consoles = new ArrayList<Console>();
			
			if (json[0].equals("200")) {
			
				Console console = new Console();
			
			if (!json[1].contains("[")){
				JSONObject response = new JSONObject(json[1]);
				console.setIdConsole(response.getJSONObject("console").getInt("idConsole"));
				console.setNomeConsole(response.getJSONObject("console").getString("nomeConsole"));
				console.setValorMonetario(Float.valueOf(response.getJSONObject("console").getString("valorMonetario")));
				consoles.add(console);
			}else{
				JSONObject response = new JSONObject(json[1]);
				JSONArray responseArray = response.getJSONArray("console");
				for (int i = 0; i < responseArray.length(); i++) {
					JSONObject responseObject = (JSONObject) responseArray.get(i);
					console = new Console();
					console.setIdConsole(responseObject.getInt("idConsole"));
					console.setNomeConsole(responseObject.getString("nomeConsole"));
					console.setValorMonetario(Float.valueOf(responseObject.getString("valorMonetario")));
					consoles.add(console);
				}
			}
			return consoles;
				
			}
			else {
				throw new Exception(json[1]);
			}	
		}
	
	public static List<Console> listaConsolesAptosDesconto() throws Exception {
		
		String[] json = new WebService().get(URI + "/Restful/usuario/listarConsolesAptosDesconto");
		
		List<Console> consoles = new ArrayList<Console>();
		
		if (json[0].equals("200")) {
		
			Console console = new Console();
		
		if (!json[1].contains("[")){
			JSONObject response = new JSONObject(json[1]);
			console.setIdConsole(response.getJSONObject("console").getInt("idConsole"));
			console.setNomeConsole(response.getJSONObject("console").getString("nomeConsole"));
			console.setValorMonetario(Float.valueOf(response.getJSONObject("console").getString("valorMonetario")));
			consoles.add(console);
		}else{
			JSONObject response = new JSONObject(json[1]);
			JSONArray responseArray = response.getJSONArray("console");
			for (int i = 0; i < responseArray.length(); i++) {
				JSONObject responseObject = (JSONObject) responseArray.get(i);
				console = new Console();
				console.setIdConsole(responseObject.getInt("idConsole"));
				console.setNomeConsole(responseObject.getString("nomeConsole"));
				console.setValorMonetario(Float.valueOf(responseObject.getString("valorMonetario")));
				consoles.add(console);
			}
		}
		return consoles;
			
		}
		else {
			throw new Exception(json[1]);
		}	
	}
	
}