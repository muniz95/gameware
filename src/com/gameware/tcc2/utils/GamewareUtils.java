package com.gameware.tcc2.utils;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.widget.EditText;

@SuppressLint("SimpleDateFormat")
public class GamewareUtils {
	
    private static void eraseTime(Calendar cal)  
    {  
        cal.set(Calendar.HOUR, 0);  
        cal.set(Calendar.MINUTE, 0);  
        cal.set(Calendar.SECOND, 0);  
        cal.set(Calendar.MILLISECOND, 0);          
    }  
      
    public static long retornaDiaSemana(long dateLong, Date date, boolean isPrimeiroDia ) {  
        final long millisPorDia = 1000 * 60 * 60 * 24;  

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        long time = dateLong;
        int diaDaSemana = cal.get(Calendar.DAY_OF_WEEK); 
  
        //Calculo do primeiro dia da semana  
        long primeiroDiaDaSemana = time - ((diaDaSemana - Calendar.SUNDAY) * millisPorDia);  
        Calendar primeiroDia = Calendar.getInstance();  
        primeiroDia.setTimeInMillis(primeiroDiaDaSemana);  
        eraseTime(primeiroDia);  
          
        //Calculo do �ltimo dia da semana  
        long ultimoDiaDaSemana = time + ((Calendar.SATURDAY - diaDaSemana) * millisPorDia);  
        Calendar ultimoDia = Calendar.getInstance();  
        ultimoDia.setTimeInMillis(ultimoDiaDaSemana);  
        eraseTime(ultimoDia);  
        
        if (isPrimeiroDia){
        	return primeiroDiaDaSemana;
        }
        else{
        	return ultimoDiaDaSemana;
        }
    }
    
    public static boolean dataMaiorAtual (String dia, String mes, String ano) throws ParseException{
    	SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
    	String dataConc = dia+mes+ano;
    	Date data = sdf.parse(dataConc);
    	
    	if (data.after(new Date())){
    		return true;
    	}
    	
    	return false;
    }
    
    public static boolean dataMaiorIgualAtual (String dia, String mes, String ano, String minutos, String horas) throws ParseException{
    	SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmm");
    	String dataConc = dia+mes+ano+minutos+horas;
    	Date data = sdf.parse(dataConc);
    	
    	if (data.after(new Date())){
    		return true;
    	}
    	
    	return false;
    }
    
    public static boolean dataMaiorAtual (String mes, String ano) throws ParseException{
    	SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
    	String dataConc = "01"+mes+ano;
    	Date data = sdf.parse(dataConc);
    	
    	if (data.after(new Date())){
    		return true;
    	}
    	
    	return false;
    }
    
    public static boolean dataMaiorAtual (String ano) throws ParseException{
    	SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
    	String dataConc = "01"+"01"+ano;
    	Date data = sdf.parse(dataConc);
    	
    	if (data.after(new Date())){
    		return true;
    	}
    	
    	return false;
    }
    
    public static String adicionaZeroEsquerdaDiaMes (int originalNumber){
    	String number = String.valueOf(originalNumber);
    	if (number.length()==1){
    		number = 0+number;
    	}
    	return number;
    }
    
	public static boolean validaData(String data) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		df.setLenient(false);
		try {
			df.parse(data);
		} catch (ParseException ex) {
			System.out.println(ex);
			return false;
		}
		return true;
	}
	
	public static boolean campoVazio(EditText campo){
		if ("".equals(campo.getText().toString())){
			return true;
		}
		return false;
	}
	
	public static String stringEditText(EditText campo){
		if (!GamewareUtils.campoVazio(campo)){
			return campo.getText().toString();
		}
			return "";
		
	}
	
	public static String formatValueDecimal(final String value) throws IllegalArgumentException {
		
		String valueAux = value.replace(",",Constants.EMPTY);
		valueAux = valueAux.replace(".",Constants.EMPTY);
		if (value.isEmpty()) {
			valueAux = "0";
		}
		BigDecimal bigDecimalValue = new BigDecimal(valueAux);  
		DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(new Locale("pt","BR")));  
		String s = df.format(bigDecimalValue.divide(new BigDecimal("100.00")));  
		return s;
	}
	
	public static String stringToBytesMD5(String password){
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		md.update(password.getBytes());
		byte[] hashMd5 = md.digest();
		String passwordMD5 = bytesToHexa(hashMd5);
		return passwordMD5;
	}
	
	public static String bytesToHexa(byte[] bytes) {
		   StringBuilder s = new StringBuilder();
		   for (int i = 0; i < bytes.length; i++) {
		       int parteAlta = ((bytes[i] >> 4) & 0xf) << 4;
		       int parteBaixa = bytes[i] & 0xf;
		       if (parteAlta == 0) s.append('0');
		       s.append(Integer.toHexString(parteAlta | parteBaixa));
		   }
		   return s.toString();
		}
	
}