package com.gameware.tcc2;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.gameware.tcc2.utils.Constants;

public class TelaSelecaoConsolesParticipantesCadastro extends ListActivity {

	private String[] listConsoles;
	ListView lView;
	public static String lstConsolesSelecionados;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent it = this.getIntent();
		Bundle bundle = it.getExtras();
		String consolesSelecionadosSessao = bundle.getString(Constants.SESSION_CONSOLES_PARTICIPANTES);
		lstConsolesSelecionados = consolesSelecionadosSessao;
		Integer sessionNovaManutencao = bundle.getInt(Constants.SESSION_NOVA_MANUTENCAO);
		if (sessionNovaManutencao==Constants.NOVA_MANUT){
			consolesSelecionadosSessao = "";
		}
		listConsoles = new String[] {"Todos","Atari","Mega-Drive","Nintendo","Saturn"};
		
		// Alterando o tipo da lista para simple_list_item_checked
		this.setListAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_checked, listConsoles));

		lView = getListView();

		// Ativando a sele��o de mais de um item na lista
		lView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

			String[] lstConsolesSelecionadosAux = consolesSelecionadosSessao.split(",");
			if (lstConsolesSelecionadosAux.length==listConsoles.length-1){
				lstConsolesSelecionadosAux = new String[]{"Todos"};
			}
			for (int i = 0; i < lstConsolesSelecionadosAux.length; i++) {
				String selecionado = lstConsolesSelecionadosAux[i];
				for (int j = 0; j < listConsoles.length; j++) {
					if (listConsoles[j].equals(selecionado.trim())){
						lView.setItemChecked(j,true);
					}
				}
			}
		
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		String selecionados = "";


		if (position!=0){
			lView.setItemChecked(0, false);
		}
		
		/*Se selecionar todos, desmarca os outros*/
		if (lView.isItemChecked(0)){
			System.out.println("Todos");
			for (int i = 1; i < lView.getCount(); i++) {
				lView.setItemChecked(i, false);
			}
			/*Sen�o, desmarca o todos*/
		}
		
		// Este for vai percorrer toda a lista sempre
		// que houver um click
		for (int i = 0; i < lView.getCount(); i++) {

			// Esta condi��o verifica se o item est� checado
			if (lView.isItemChecked(i) == true) {

				if (selecionados.equals("")) {
					selecionados += listConsoles[i];
				} else {
					selecionados += ", " + listConsoles[i];
				}
			}
		}

		Toast.makeText(this, "Console(s) escolhido(s): " + selecionados,
				Toast.LENGTH_SHORT).show();
	}

	// Evento executado quando finalizar(clicar no bot�o voltar) a Intent
	@Override
	protected void onDestroy() {
		System.out.println("onDestroy - TelaSelecao");
		super.onDestroy();
		lstConsolesSelecionados = "";

		for (int i = 0; i < lView.getCount(); i++) {

			// Esta condi��o verifica se o item est� checado
			if (lView.isItemChecked(i) == true) {

				if (lstConsolesSelecionados.equals("")) {
					lstConsolesSelecionados += listConsoles[i];
				} else {
					lstConsolesSelecionados += ", " + listConsoles[i];
				}
			}
		}
		
		if ("Todos".equals(lstConsolesSelecionados)){
			lstConsolesSelecionados = "";
			for (int i = 1; i < lView.getCount(); i++) {
				// pega os itens selecionados
				if (lView.getCount()- 1 == i) {
					lstConsolesSelecionados += listConsoles[i];
				} else {
					lstConsolesSelecionados += listConsoles[i] + ",";
				}
			}	
		}
		
		System.out.println("Saida do metodo onDestroy TelaSelecaoConsolesParticipantes: "+lstConsolesSelecionados);
		Intent it = this.getIntent();
		Bundle bundle = it.getExtras();
		if (bundle==null){
			bundle = new Bundle();
		}
		bundle.putString(Constants.SESSION_CONSOLES_PARTICIPANTES, lstConsolesSelecionados);
		bundle.putInt(Constants.SESSION_NOVA_MANUTENCAO, Constants.NOT_NOVA_MANUT);
	}
	
	 @Override
	    public void onStart() {
	        super.onStart();
	       System.out.println("onStart - TelaSelecao");
	    }

	   @Override
	    public void onResume() {
	        super.onResume();
	       System.out.println("onResume - TelaSelecao");
	    }

	    @Override
	    public void onPause() {
	        super.onPause();
	        System.out.println("onPause - TelaSelecao");
	    }

	    @Override
	    public void onStop() {
	        super.onStop();
	       System.out.println("onStop - TelaSelecao");
	    }

}