﻿-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- PostgreSQL version: 9.1
-- Project Site: pgmodeler.com.br
-- Model Author: ---

SET check_function_bodies = false;
-- ddl-end --


-- Database creation must be done outside an multicommand file.
-- These commands were put in this file only for convenience.
-- -- object: novo_banco_de_dados | type: DATABASE --
-- CREATE DATABASE novo_banco_de_dados
-- ;
-- -- ddl-end --
-- 

-- object: public."Contato" | type: TABLE --
CREATE TABLE public.Contato(
	idContato serial NOT NULL,
	email character varying(25) NOT NULL,
	telefone character varying(16) NOT NULL,
	celular character varying(16),
	skype character varying(25),
	facebook character varying(25),
	gtalk character varying(50),
	whatsapp character varying(20),
	idCliente integer,
	idUsuario integer,
	CONSTRAINT idContato PRIMARY KEY (idContato)

);
-- ddl-end --
-- object: public."Cliente" | type: TABLE --
CREATE TABLE public.Cliente(
	idCliente serial NOT NULL,
	nome character varying(50) NOT NULL,
	idade integer NOT NULL,
	sexo char NOT NULL,
	dataNascimento date NOT NULL,
	rg integer NOT NULL,
	cpf bigint NOT NULL,
	endRua character varying(50) NOT NULL,
	endNum integer NOT NULL,
	endCompl character varying(10),
	endBairro character varying(30) NOT NULL,
	endCep character varying(9) NOT NULL,
	endCidade character varying(50) NOT NULL,
	endEstado character varying(2) NOT NULL,
	CONSTRAINT idCliente PRIMARY KEY (idCliente)

);
-- ddl-end --
-- object: public."Console" | type: TABLE --
CREATE TABLE public.Console(
	idConsole serial NOT NULL,
	nomeConsole character varying(20) NOT NULL,
	valorMonetario float NOT NULL,
	CONSTRAINT idConsole PRIMARY KEY (idConsole)

);
-- ddl-end --
-- object: public."Equipamento" | type: TABLE --
CREATE TABLE public.Equipamento(
	idEquipamento serial NOT NULL,
	nome character varying(50) NOT NULL,
	isAssociadoConsole boolean,
	emUso boolean,
	qtdEstoque integer NOT NULL,
	idPreco integer NOT NULL,
	idTipoEquipamento integer NOT NULL,
	idConsole integer NOT NULL,
	idDesconto integer,
	CONSTRAINT idEquipamento PRIMARY KEY (idEquipamento)

);
-- ddl-end --
-- object: public."Locacao" | type: TABLE --
CREATE TABLE public.Locacao(
	idLocacao serial NOT NULL,
	valorGasto float NOT NULL,
	inicioLocacao date NOT NULL,
	fimLocacao date NOT NULL,
	idEquipamento integer NOT NULL,
	idCliente integer NOT NULL,
	CONSTRAINT idLocacao PRIMARY KEY (idLocacao)

);
-- ddl-end --
-- object: public."Preco" | type: TABLE --
CREATE TABLE public.Preco(
	idPreco serial NOT NULL,
	inicioValidadePreco date NOT NULL,
	fimValidadePreco date NOT NULL,
	valorMonetario float NOT NULL,
	CONSTRAINT idPreco PRIMARY KEY (idPreco)

);
-- ddl-end --
-- object: public."TipoEquipamento" | type: TABLE --
CREATE TABLE public.TipoEquipamento(
	idTipoEquipamento serial NOT NULL,
	nomeTipo character varying(20) NOT NULL,
	CONSTRAINT idTipoEquipamento PRIMARY KEY (idTipoEquipamento)

);
-- ddl-end --
-- object: public."Usuario" | type: TABLE --
CREATE TABLE public.Usuario(
	idUsuario serial NOT NULL,
	login character varying(25) NOT NULL,
	senha character varying(25) NOT NULL,
	tipoUsuario character varying(15) NOT NULL,
	CONSTRAINT idUsuario PRIMARY KEY (idUsuario)

);
-- ddl-end --
-- object: "Preco_fk" | type: CONSTRAINT --
ALTER TABLE public.Equipamento ADD CONSTRAINT Preco_fk FOREIGN KEY (idPreco)
REFERENCES public.Preco (idPreco) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "TipoEquipamento_fk" | type: CONSTRAINT --
ALTER TABLE public.Equipamento ADD CONSTRAINT TipoEquipamento_fk FOREIGN KEY (idTipoEquipamento)
REFERENCES public.TipoEquipamento (idTipoEquipamento) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "Equipamento_fk" | type: CONSTRAINT --
ALTER TABLE public.Locacao ADD CONSTRAINT Equipamento_fk FOREIGN KEY (idEquipamento)
REFERENCES public.Equipamento (idEquipamento) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "Console_fk" | type: CONSTRAINT --
ALTER TABLE public.Equipamento ADD CONSTRAINT Console_fk FOREIGN KEY (idConsole)
REFERENCES public.Console (idConsole) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "Cliente_fk" | type: CONSTRAINT --
ALTER TABLE public.Locacao ADD CONSTRAINT Cliente_fk FOREIGN KEY (idCliente)
REFERENCES public.Cliente (idCliente) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "Cliente_fk" | type: CONSTRAINT --
ALTER TABLE public.Contato ADD CONSTRAINT Cliente_fk FOREIGN KEY (idCliente)
REFERENCES public.Cliente (idCliente) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "Usuario_fk" | type: CONSTRAINT --
ALTER TABLE public.Contato ADD CONSTRAINT Usuario_fk FOREIGN KEY (idUsuario)
REFERENCES public.Usuario (idUsuario) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: public."Desconto" | type: TABLE --
CREATE TABLE public.Desconto(
	idDesconto serial NOT NULL,
	porcentagemDesconto integer NOT NULL,
	inicioValidade integer NOT NULL,
	fimValidade integer NOT NULL,
	nomeDesconto character varying(25) NOT NULL,
	CONSTRAINT idDesconto PRIMARY KEY (idDesconto)

);
-- ddl-end --
-- object: "Desconto_fk" | type: CONSTRAINT --
ALTER TABLE public.Equipamento ADD CONSTRAINT Desconto_fk FOREIGN KEY (idDesconto)
REFERENCES public.Desconto (idDesconto) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --



